package prometheus

import (
	"geekgo/web"
	"math/rand"
	"testing"
	"time"
)

func TestMiddleware(t *testing.T) {
	builder := &MiddlewareBuilder{
		Namespace: "geekgo",
		Subsystem: "promet",
		Name:      "test",
	}
	server := web.NewHTTPServer(web.ServerWithMiddleware(builder.Build()))
	server.Get("/user", func(ctx *web.Context) {
		time.Sleep(time.Duration(rand.Intn(1000)+1) * time.Millisecond)
		ctx.RespJSON(200, struct{ Name string }{Name: "Bob"})
	})

	// go func() {
	// 	http.Handle("/metrics", promhttp.Handler())
	// 	http.ListenAndServe(":8082", nil)
	// }()
	// server.Start(":8081")

}
