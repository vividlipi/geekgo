package accesslog_test

import (
	"encoding/json"
	"geekgo/web"
	"geekgo/web/middlewares/accesslog"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAccessLog_ServerOption(t *testing.T) {
	want := accesslog.RecordLog{
		Host:   "localhost",
		Route:  "/a/b/*",
		Method: http.MethodGet,
		Path:   "/a/b/c",
	}
	builder := accesslog.MiddlewareBuilder{}
	info := strings.Builder{}
	builder.LogFunc(func(log string) {
		info.WriteString(log)
	})

	opts := web.ServerWithMiddleware(builder.Build())
	s := web.NewHTTPServer(opts)
	s.Get(want.Route, func(ctx *web.Context) {})

	req := httptest.NewRequest(http.MethodGet, want.Path, nil)
	req.Host = want.Host
	s.ServeHTTP(httptest.NewRecorder(), req)

	excepted, _ := json.Marshal(want)
	assert.Equal(t, string(excepted), info.String())
}
