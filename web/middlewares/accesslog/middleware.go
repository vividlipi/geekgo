package accesslog

import (
	"encoding/json"
	"geekgo/web"
)

type MiddlewareBuilder struct {
	logFunc func(log string)
}

func (m *MiddlewareBuilder) LogFunc(fn func(log string)) *MiddlewareBuilder {
	m.logFunc = fn
	return m
}

func (m MiddlewareBuilder) Build() web.Middleware {
	return func(hf web.HandleFunc) web.HandleFunc {
		return func(ctx *web.Context) {
			defer func() {
				info := RecordLog{
					Host:   ctx.Req.Host,
					Route:  ctx.MatchedRoute,
					Method: ctx.Req.Method,
					Path:   ctx.Req.URL.Path,
				}
				data, _ := json.Marshal(info)
				m.logFunc(string(data))
			}()
			hf(ctx)
		}
	}
}

type RecordLog struct {
	Host   string `json:"host"`
	Route  string `json:"route"`
	Method string `json:"method"`
	Path   string `json:"path"`
}
