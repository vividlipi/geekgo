package opentelemetry

import (
	"geekgo/web"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

const instrumentationName = "geekgo/middlewares/opentelemetry"

type MiddlewareBuilder struct {
	Tracer trace.Tracer
}

func (m *MiddlewareBuilder) Build() web.Middleware {
	if m.Tracer == nil {
		m.Tracer = otel.GetTracerProvider().Tracer(instrumentationName)
	}
	return func(hf web.HandleFunc) web.HandleFunc {
		return func(ctx *web.Context) {
			// 尝试和客户端的tracer 结合在一起
			reqCtx := otel.GetTextMapPropagator().Extract(ctx.Req.Context(), propagation.HeaderCarrier(ctx.Req.Header))

			_, span := m.Tracer.Start(reqCtx, "unknow")
			defer span.End()

			span.SetAttributes(attribute.String("http.method", ctx.Req.Method))
			span.SetAttributes(attribute.String("http.url", ctx.Req.URL.String()))

			ctx.Req = ctx.Req.WithContext(reqCtx)

			hf(ctx)

			span.SetName(ctx.MatchedRoute)
		}
	}
}
