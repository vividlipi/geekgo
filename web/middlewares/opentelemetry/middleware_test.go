package opentelemetry

import (
	"geekgo/web"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"go.opentelemetry.io/otel"
)

func TestMiddleware(t *testing.T) {
	tracer := otel.GetTracerProvider().Tracer(instrumentationName)
	builder := MiddlewareBuilder{
		Tracer: tracer,
	}
	server := web.NewHTTPServer(web.ServerWithMiddleware(builder.Build()))

	server.Get("/user", func(ctx *web.Context) {
		c, span := tracer.Start(ctx.Req.Context(), "first_layer")
		defer span.End()

		s_c, s_span := tracer.Start(c, "second_layer_1")
		time.Sleep(time.Second)
		s_span.End()

		_, s_span = tracer.Start(c, "second_layer_2")
		time.Sleep(time.Second)
		s_span.End()

		_, t_span := tracer.Start(s_c, "third_layer")
		time.Sleep(time.Second)
		t_span.End()

		time.Sleep(100 * time.Millisecond)
		ctx.RespJSON(202, struct{ Name string }{Name: "Tom"})
	})

	req := httptest.NewRequest(http.MethodGet, "/user", nil)
	res := httptest.NewRecorder()

	server.ServeHTTP(res, req)
}
