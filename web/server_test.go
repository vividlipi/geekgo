package web

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMiddlewareInvoke(t *testing.T) {
	h := NewHTTPServer()
	h.mdls = []Middleware{
		func(hf HandleFunc) HandleFunc {
			return func(ctx *Context) {
				ctx.Resp.Write([]byte("B1"))
				hf(ctx)
				ctx.Resp.Write([]byte("A1"))
			}
		},
		func(hf HandleFunc) HandleFunc {
			return func(ctx *Context) {
				ctx.Resp.Write([]byte("B2"))
				hf(ctx)
				ctx.Resp.Write([]byte("A2"))
			}
		},
	}
	h.Get("/a/b/c", func(ctx *Context) {
		ctx.Resp.Write([]byte("/a/b/c"))
	})
	req := httptest.NewRequest(http.MethodGet, "/a/b/c", nil)
	res := httptest.NewRecorder()
	h.ServeHTTP(res, req)
	assert.Equal(t, "B1B2/a/b/cA2A1", res.Body.String())
}
