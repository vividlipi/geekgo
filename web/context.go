package web

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
)

type Context struct {
	Req            *http.Request
	Resp           http.ResponseWriter
	RespData       []byte
	RespStatusCode int
	param          map[string]string
	cacheQuery     url.Values
	MatchedRoute   string
	RenderEngine   RenderEngine
}

func (c *Context) SetCookie(ck *http.Cookie) {
	http.SetCookie(c.Resp, ck)
}

func (c *Context) RespJSON(status int, val any) error {
	result, err := json.Marshal(val)
	if err != nil {
		return err
	}
	c.Resp.Header().Set("Content-Type", "application/json")
	c.RespData = result
	c.RespStatusCode = status
	return nil
}

func (c *Context) BindJSON(val any) error {
	decoder := json.NewDecoder(c.Req.Body)
	return decoder.Decode(val)
}

func (c *Context) FormValue(key string) StringValue {
	if err := c.Req.ParseForm(); err != nil {
		return StringValue{"", err}
	}

	if values, ok := c.Req.Form[key]; ok {
		return StringValue{values[0], nil}
	}
	return StringValue{"", errors.New("key not exist in form")}
}

func (c *Context) QueryValue(key string) StringValue {
	if c.cacheQuery == nil {
		c.cacheQuery = c.Req.URL.Query()
	}

	if values, ok := c.cacheQuery[key]; ok {
		return StringValue{values[0], nil}
	}

	return StringValue{"", errors.New("key not exist in query")}
}

func (c *Context) PathValue(key string) StringValue {
	if value, ok := c.param[key]; ok {
		return StringValue{value, nil}
	}

	return StringValue{"", errors.New("key not exist in path")}
}

type StringValue struct {
	value string
	err   error
}

func (s StringValue) ToInt64() (int64, error) {
	if s.err != nil {
		return 0, s.err
	}
	return strconv.ParseInt(s.value, 10, 64)
}

func (s StringValue) ToFloat64() (float64, error) {
	if s.err != nil {
		return 0, s.err
	}
	return strconv.ParseFloat(s.value, 64)
}

func (s StringValue) ToString() (string, error) {
	return s.value, s.err
}
