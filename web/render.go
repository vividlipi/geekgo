package web

import (
	"bytes"
	"context"
	"html/template"
)

type RenderEngine interface {
	Render(ctx context.Context, tmpl string, data any) ([]byte, error)
}

type GoRenderEngine struct {
	Template *template.Template
}

func (e *GoRenderEngine) Render(ctx context.Context, tmlp string, data any) ([]byte, error) {
	buf := &bytes.Buffer{}
	err := e.Template.ExecuteTemplate(buf, tmlp, data)
	return buf.Bytes(), err
}
