package web

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBindJSON(t *testing.T) {
	type user struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}
	data := user{
		Name: "王二",
		Age:  18,
	}
	requestBody, _ := json.Marshal(data)
	t.Run("bind user", func(t *testing.T) {
		request := httptest.NewRequest(http.MethodPost, "/user", bytes.NewBuffer(requestBody))
		context := &Context{Req: request}
		got := user{}
		context.BindJSON(&got)
		assert.Equal(t, data.Name, got.Name)
		assert.Equal(t, data.Age, got.Age)
	})
	t.Run("data can only read once", func(t *testing.T) {
		request := httptest.NewRequest(http.MethodPost, "/user", bytes.NewBuffer(requestBody))
		context := &Context{Req: request}
		got := user{}
		context.BindJSON(&user{})
		context.BindJSON(&got)
		assert.Equal(t, "", got.Name)
		assert.Equal(t, 0, got.Age)
	})
	t.Run("bind to nil", func(t *testing.T) {
		request := httptest.NewRequest(http.MethodPost, "/user", bytes.NewBuffer(requestBody))
		context := &Context{Req: request}
		if err := context.BindJSON(nil); err == nil {
			t.Errorf("want an error cause try bind to nil")
		}
	})
}

func TestFormValue(t *testing.T) {
	t.Run("get Form value from post body", func(t *testing.T) {
		form := url.Values{}
		form.Add("name", "Alice")
		form.Add("age", "18")

		req, _ := http.NewRequest(http.MethodPost, "/form", strings.NewReader(form.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		context := &Context{Req: req}
		stringValue := context.FormValue("name")

		assert.Nil(t, stringValue.err)
		assert.Equal(t, "Alice", stringValue.value)
	})
	t.Run("get Form value from request url", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/form?name=Alice", nil)

		context := &Context{Req: req}
		stringValue := context.FormValue("name")

		assert.Nil(t, stringValue.err)
		assert.Equal(t, "Alice", stringValue.value)
	})
	t.Run("get not exist key will return an error", func(t *testing.T) {
		form := url.Values{}
		form.Add("name", "Alice")
		form.Add("age", "18")

		req, _ := http.NewRequest(http.MethodPost, "/form?name=王二", strings.NewReader(form.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		context := &Context{Req: req}
		stringValue := context.FormValue("city")
		assert.NotNil(t, stringValue.err)
	})
	t.Run("get Form value from request url", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/form?name=", nil)

		context := &Context{Req: req}
		stringValue := context.FormValue("name")

		assert.Nil(t, stringValue.err)
		assert.Equal(t, "", stringValue.value)
	})
}

func TestQueryValue(t *testing.T) {
	t.Run("get value in query", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/form?name=Alice&name=Bob&age=18", nil)
		context := &Context{Req: req}
		s := context.QueryValue("name")
		assert.Nil(t, s.err)
		assert.Equal(t, "Alice", s.value)
	})
	t.Run("got error when key not in query", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/form?name=Alice&name=Bob&age=18", nil)
		context := &Context{Req: req}
		s := context.QueryValue("city")
		assert.NotNil(t, s.err)
	})
	t.Run("the following query will access cache", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/form?name=Alice&name=Bob&age=18", nil)
		context := &Context{Req: req}
		s := context.QueryValue("age")
		assert.Equal(t, "18", s.value)
		assert.NotNil(t, context.cacheQuery)
		context.cacheQuery = url.Values{"age": []string{"20"}}
		s = context.QueryValue("age")
		assert.Equal(t, "20", s.value)
	})
}

func TestPathValue(t *testing.T) {
	context := &Context{
		param: map[string]string{
			"category": "book",
		},
	}
	t.Run("get value in path", func(t *testing.T) {
		s := context.PathValue("category")
		assert.Nil(t, s.err)
		assert.Equal(t, "book", s.value)
	})
	t.Run("got error when key not in path", func(t *testing.T) {
		s := context.PathValue("cate")
		assert.NotNil(t, s.err)
	})
}

func TestRespJSON(t *testing.T) {
	t.Run("response an object json", func(t *testing.T) {
		record := httptest.NewRecorder()
		context := &Context{Resp: record}

		type user struct {
			Name string `json:"name"`
			Age  int    `json:"age"`
		}
		data := user{
			Name: "王二",
			Age:  18,
		}

		err := context.RespJSON(http.StatusOK, data)

		assert.Nil(t, err)
		assert.Equal(t, http.StatusOK, context.RespStatusCode)
		assert.Equal(t, "application/json", record.Header()["Content-Type"][0])
		want, _ := json.Marshal(data)
		assert.Equal(t, string(want), string(context.RespData))
	})
}
