package web

import (
	"fmt"
	"math/rand"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func getBenchMarkRouter() *Router {
	r := NewRoute()
	mockHandler := func(ctx *Context) {}
	routes := []struct {
		method string
		path   string
	}{
		{http.MethodGet, "/"},
		{http.MethodGet, "/user"},
		{http.MethodGet, "/user/:id(^[0-9]+$)"},
		{http.MethodGet, "/user/:id(^[0-9]+$)/article"},
		{http.MethodGet, "/user/:id(^[0-9]+$)/article/:page(^[0-9]+$)"},
		{http.MethodGet, "/article"},
		{http.MethodGet, "/article/category"},
		{http.MethodGet, "/article/category/*"},
		{http.MethodGet, "/article/category/*/:page(^[0-9]+$)"},
		{http.MethodGet, "/blog/:topic"},
		{http.MethodGet, "/blog/:topic/:page(^[0-9]+$)"},
	}

	for _, route := range routes {
		r.AddRoute(route.method, route.path, mockHandler)
	}

	return r
}

func assertEqualRouter(t testing.TB, got *Router, wanted *Router) {
	t.Helper()

	if got == nil && wanted == nil {
		return
	}

	if got == nil || wanted == nil {
		t.Fatalf("One of got or wanted is nil")
	}

	if msg, ok := wanted.equalTo(got); !ok {
		t.Errorf(msg)
	}
}

func (r *Router) equalTo(o *Router) (string, bool) {
	if o == nil {
		return "Other Router is Nil", false
	}

	if len(r.trees) != len(o.trees) {
		return "routers has diff len of trees", false
	}

	for k, v := range r.trees {
		node, ok := o.trees[k]
		if !ok {
			return fmt.Sprintf("method tree %s Not Exist in Router", k), false
		}
		msg, equal := v.equalTo(node)
		if !equal {
			return msg, false
		}
	}

	return "", true
}

func (n *Node) equalTo(o *Node) (string, bool) {
	if o == nil {
		return "Other Node is Nil", false
	}

	if n.path != o.path {
		return fmt.Sprintf("path %q is not equal to %q", n.path, o.path), false
	}

	nHandler := reflect.ValueOf(n.handler)
	oHandler := reflect.ValueOf(o.handler)

	if nHandler != oHandler {
		return fmt.Sprintf("Node %q Handler is Not the Same", o.path), false
	}

	if len(n.children) != len(o.children) {
		return fmt.Sprintf("%v len children not the same with %v", n.children, o.children), false
	}

	for key, value := range n.children {
		node, ok := o.children[key]
		if !ok {
			return fmt.Sprintf("key %q is not exist in the other Node", key), false
		}
		msg, equal := value.equalTo(node)
		if !equal {
			return msg, false
		}
	}

	if n.wildcard != nil {
		if msg, equal := n.wildcard.equalTo(o.wildcard); !equal {
			return msg, equal
		}
	}

	if n.param != nil {
		if msg, equal := n.param.equalTo(o.param); !equal {
			return msg, equal
		}
	}

	if n.regmod != nil {
		if o.regmod == nil {
			return fmt.Sprintf("regmod want: %s but got: nil", n.regmod.String()), false
		}
		if n.regmod.String() != o.regmod.String() {
			return fmt.Sprintf("regmod want: %s but got: %s", n.regmod.String(), o.regmod.String()), false
		}
		if msg, equal := n.regexp.equalTo(o.regexp); !equal {
			return msg, equal
		}
	}

	if n.mdls == nil {
		if o.mdls != nil {
			return fmt.Sprintf("want nil got something len of got: %d", len(o.mdls)), false
		}
	}

	if len(n.mdls) != len(o.mdls) {
		return fmt.Sprintf("want has %d middlewares but got %d of it", len(n.mdls), len(o.mdls)), false
	}

	for i, m := range n.mdls {
		fw := reflect.ValueOf(m)
		fg := reflect.ValueOf(o.mdls[i])
		if fw != fg {
			return fmt.Sprintf("the %dth middleware is not the same", i), false
		}
	}

	return "", true
}

func TestRegist_Static(t *testing.T) {
	t.Run("One secondary Path", func(t *testing.T) {
		path := "/user/home"
		var mockHandler HandleFunc = func(ctx *Context) {}

		router := NewRoute()
		router.AddRoute(http.MethodGet, path, mockHandler)

		homeNode := &Node{
			path:    "home",
			handler: mockHandler,
		}

		userNode := &Node{
			path: "user",
			children: map[string]*Node{
				"home": homeNode,
			},
		}

		root := &Node{
			path: "/",
			children: map[string]*Node{
				"user": userNode,
			},
		}

		wanted := &Router{
			trees: map[string]*Node{
				http.MethodGet: root,
			},
		}

		assertEqualRouter(t, router, wanted)
	})
	t.Run("Multi secondary Path", func(t *testing.T) {
		var mockHandler HandleFunc = func(ctx *Context) {}

		router := NewRoute()
		router.AddRoute(http.MethodGet, "/user/home", mockHandler)
		router.AddRoute(http.MethodGet, "/user/space", mockHandler)

		homeNode := &Node{
			path:    "home",
			handler: mockHandler,
		}

		spaceNode := &Node{
			path:    "space",
			handler: mockHandler,
		}

		userNode := &Node{
			path: "user",
			children: map[string]*Node{
				"home":  homeNode,
				"space": spaceNode,
			},
		}

		root := &Node{
			path: "/",
			children: map[string]*Node{
				"user": userNode,
			},
		}

		wanted := &Router{
			trees: map[string]*Node{
				http.MethodGet: root,
			},
		}

		assertEqualRouter(t, router, wanted)
	})
	t.Run("Multi secondary Path diff", func(t *testing.T) {
		var mockHandler HandleFunc = func(ctx *Context) {}

		router := NewRoute()
		router.AddRoute(http.MethodGet, "/user/home", mockHandler)
		router.AddRoute(http.MethodGet, "/user/space", mockHandler)
		router.AddRoute(http.MethodGet, "/admin/home", mockHandler)
		router.AddRoute(http.MethodGet, "/admin/space", mockHandler)

		homeNode := &Node{
			path:    "home",
			handler: mockHandler,
		}

		spaceNode := &Node{
			path:    "space",
			handler: mockHandler,
		}

		userNode := &Node{
			path: "user",
			children: map[string]*Node{
				"home":  homeNode,
				"space": spaceNode,
			},
		}

		adminNode := &Node{
			path: "admin",
			children: map[string]*Node{
				"home":  homeNode,
				"space": spaceNode,
			},
		}

		root := &Node{
			path: "/",
			children: map[string]*Node{
				"user":  userNode,
				"admin": adminNode,
			},
		}

		wanted := &Router{
			trees: map[string]*Node{
				http.MethodGet: root,
			},
		}

		assertEqualRouter(t, router, wanted)
	})
	t.Run("diff method", func(t *testing.T) {
		var mockHandler HandleFunc = func(ctx *Context) {}

		router := NewRoute()
		router.AddRoute(http.MethodGet, "/user/home", mockHandler)
		router.AddRoute(http.MethodGet, "/user/space", mockHandler)
		router.AddRoute(http.MethodPost, "/user/home", mockHandler)
		router.AddRoute(http.MethodPost, "/user/space", mockHandler)

		homeNode := &Node{
			path:    "home",
			handler: mockHandler,
		}

		spaceNode := &Node{
			path:    "space",
			handler: mockHandler,
		}

		userNode := &Node{
			path: "user",
			children: map[string]*Node{
				"home":  homeNode,
				"space": spaceNode,
			},
		}

		root := &Node{
			path: "/",
			children: map[string]*Node{
				"user": userNode,
			},
		}

		wanted := &Router{
			trees: map[string]*Node{
				http.MethodGet:  root,
				http.MethodPost: root,
			},
		}

		assertEqualRouter(t, router, wanted)
	})
	t.Run("diff deepth path", func(t *testing.T) {
		var mockHandler HandleFunc = func(ctx *Context) {}

		router := NewRoute()
		router.AddRoute(http.MethodGet, "/user/home", mockHandler)
		router.AddRoute(http.MethodGet, "/user", mockHandler)
		router.AddRoute(http.MethodGet, "/", mockHandler)

		homeNode := &Node{
			path:    "home",
			handler: mockHandler,
		}

		userNode := &Node{
			path: "user",
			children: map[string]*Node{
				"home": homeNode,
			},
			handler: mockHandler,
		}

		root := &Node{
			path: "/",
			children: map[string]*Node{
				"user": userNode,
			},
			handler: mockHandler,
		}

		wanted := &Router{
			trees: map[string]*Node{
				http.MethodGet: root,
			},
		}

		assertEqualRouter(t, router, wanted)
	})
	t.Run("diff deepth path 2", func(t *testing.T) {
		var mockHandler HandleFunc = func(ctx *Context) {}

		router := NewRoute()
		router.AddRoute(http.MethodGet, "/user/home", mockHandler)
		router.AddRoute(http.MethodGet, "/", mockHandler)

		homeNode := &Node{
			path:    "home",
			handler: mockHandler,
		}

		userNode := &Node{
			path: "user",
			children: map[string]*Node{
				"home": homeNode,
			},
		}

		root := &Node{
			path: "/",
			children: map[string]*Node{
				"user": userNode,
			},
			handler: mockHandler,
		}

		wanted := &Router{
			trees: map[string]*Node{
				http.MethodGet: root,
			},
		}

		assertEqualRouter(t, router, wanted)
	})
}

func TestRegist_Wildcard(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}

	router := NewRoute()
	router.AddRoute(http.MethodGet, "/", mockHandler)
	router.AddRoute(http.MethodGet, "/*", mockHandler)
	router.AddRoute(http.MethodGet, "/user", mockHandler)
	router.AddRoute(http.MethodGet, "/*/*", mockHandler)
	router.AddRoute(http.MethodGet, "/*/space", mockHandler)
	router.AddRoute(http.MethodGet, "/user/*", mockHandler)
	router.AddRoute(http.MethodGet, "/user/space", mockHandler)
	router.AddRoute(http.MethodGet, "/*/*/home", mockHandler)

	spaceNode := &Node{
		path:    "space",
		handler: mockHandler,
	}

	homeNode := &Node{
		path:    "home",
		handler: mockHandler,
	}

	userNode := &Node{
		path: "user",
		children: map[string]*Node{
			"space": spaceNode,
		},
		wildcard: &Node{
			path:    "*",
			handler: mockHandler,
		},
		handler: mockHandler,
	}
	rootSecondWildCardNode := &Node{
		path: "*",
		children: map[string]*Node{
			"home": homeNode,
		},
		handler: mockHandler,
	}

	rootWildCardNode := &Node{
		path: "*",
		children: map[string]*Node{
			"space": spaceNode,
		},
		wildcard: rootSecondWildCardNode,
		handler:  mockHandler,
	}

	root := &Node{
		path: "/",
		children: map[string]*Node{
			"user": userNode,
		},
		wildcard: rootWildCardNode,
		handler:  mockHandler,
	}

	wanted := &Router{
		trees: map[string]*Node{
			http.MethodGet: root,
		},
	}

	assertEqualRouter(t, router, wanted)
}

func TestRegist_Param(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}

	router := NewRoute()
	router.AddRoute(http.MethodGet, "/", mockHandler)
	router.AddRoute(http.MethodGet, "/list", mockHandler)
	router.AddRoute(http.MethodGet, "/list/:catagory", mockHandler)

	catagoryNode := &Node{
		path:    ":catagory",
		handler: mockHandler,
	}

	listNode := &Node{
		path:    "list",
		handler: mockHandler,
		param:   catagoryNode,
	}

	root := &Node{
		path: "/",
		children: map[string]*Node{
			"list": listNode,
		},
		handler: mockHandler,
	}
	wanted := &Router{
		trees: map[string]*Node{
			http.MethodGet: root,
		},
	}
	assertEqualRouter(t, router, wanted)
}
func TestRegist_RegEx(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}
	router := NewRoute()
	router.AddRoute(http.MethodGet, "/user/:number(^[0-9]+$)", mockHandler)
	router.AddRoute(http.MethodGet, "/user/:number(^[0-9]+$)/article/:index(^[0-9]+$)", mockHandler)

	numReg, _ := regexp.Compile("^[0-9]+$")

	indexRegNode := &Node{
		path:    ":index(^[0-9]+$)",
		handler: mockHandler,
	}
	articleNode := &Node{
		path:   "article",
		regmod: numReg,
		regexp: indexRegNode,
	}
	numberRegNode := &Node{
		path: ":number(^[0-9]+$)",
		children: map[string]*Node{
			"article": articleNode,
		},
		handler: mockHandler,
	}
	userNode := &Node{
		path:   "user",
		regmod: numReg,
		regexp: numberRegNode,
	}
	root := &Node{
		path: "/",
		children: map[string]*Node{
			"user": userNode,
		},
	}
	expected := &Router{
		trees: map[string]*Node{
			http.MethodGet: root,
		},
	}

	assertEqualRouter(t, router, expected)
}

func TestFind_Static(t *testing.T) {

	var mockHandler HandleFunc = func(ctx *Context) {}

	router := NewRoute()
	router.AddRoute(http.MethodGet, "/", mockHandler)
	router.AddRoute(http.MethodGet, "/user/home", mockHandler)
	router.AddRoute(http.MethodGet, "/user/space", mockHandler)
	router.AddRoute(http.MethodPost, "/user/home", mockHandler)
	router.AddRoute(http.MethodPost, "/user/space", mockHandler)

	homeNode := &Node{
		path:    "home",
		handler: mockHandler,
	}

	spaceNode := &Node{
		path:    "space",
		handler: mockHandler,
	}

	userNode := &Node{
		path: "user",
		children: map[string]*Node{
			"home":  homeNode,
			"space": spaceNode,
		},
	}

	getRoot := &Node{
		path: "/",
		children: map[string]*Node{
			"user": userNode,
		},
		handler: mockHandler,
	}

	postRoot := &Node{
		path: "/",
		children: map[string]*Node{
			"user": userNode,
		},
	}

	cases := []struct {
		name    string
		method  string
		path    string
		founded bool
		node    *Node
	}{
		{
			name:    "get root",
			method:  http.MethodGet,
			path:    "/",
			founded: true,
			node:    getRoot,
		},
		{
			name:    "post root",
			method:  http.MethodPost,
			path:    "/",
			founded: true,
			node:    postRoot,
		},
		{
			name:    "get user",
			method:  http.MethodGet,
			path:    "/user",
			founded: true,
			node:    userNode,
		},
		{
			name:    "post user/space",
			method:  http.MethodGet,
			path:    "/user/space",
			founded: true,
			node:    spaceNode,
		},
		{
			name:    "post user/space/xyz",
			method:  http.MethodPost,
			path:    "/user/space/xyz",
			founded: false,
			node:    nil,
		},
		{
			name:    "post user/spac",
			method:  http.MethodPost,
			path:    "/user/spac",
			founded: false,
			node:    nil,
		},
		{
			name:    "delete user/spac",
			method:  http.MethodDelete,
			path:    "/user/spac",
			founded: false,
			node:    nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			info, ok := router.FindRoute(tc.method, tc.path)
			if ok != tc.founded {
				t.Fatalf("path %s %q shoud %v but %v", tc.method, tc.path, tc.founded, ok)
			}
			if tc.founded {
				if msg, equal := tc.node.equalTo(info.node); !equal {
					t.Error(msg)
				}
			}
		})
	}
}

func TestFind_Wildcard(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}

	router := NewRoute()
	router.AddRoute(http.MethodGet, "/", mockHandler)
	router.AddRoute(http.MethodGet, "/*", mockHandler)
	router.AddRoute(http.MethodGet, "/user", mockHandler)
	router.AddRoute(http.MethodGet, "/*/*", mockHandler)
	router.AddRoute(http.MethodGet, "/*/space", mockHandler)
	router.AddRoute(http.MethodGet, "/user/*", mockHandler)
	router.AddRoute(http.MethodGet, "/user/space", mockHandler)
	router.AddRoute(http.MethodGet, "/*/*/home", mockHandler)

	spaceNode := &Node{
		path:    "space",
		handler: mockHandler,
	}

	homeNode := &Node{
		path:    "home",
		handler: mockHandler,
	}

	userWildCard := &Node{
		path:    "*",
		handler: mockHandler,
	}

	// userNode := &Node{
	// 	path: "user",
	// 	children: map[string]*Node{
	// 		"space": spaceNode,
	// 	},
	// 	wildcard: userWildCard,
	// 	handler:  mockHandler,
	// }
	rootSecondWildCardNode := &Node{
		path: "*",
		children: map[string]*Node{
			"home": homeNode,
		},
		handler: mockHandler,
	}

	rootWildCardNode := &Node{
		path: "*",
		children: map[string]*Node{
			"space": spaceNode,
		},
		wildcard: rootSecondWildCardNode,
		handler:  mockHandler,
	}

	// root := &Node{
	// 	path: "/",
	// 	children: map[string]*Node{
	// 		"user": userNode,
	// 	},
	// 	wildcard: rootWildCardNode,
	// 	handler:  mockHandler,
	// }

	// wanted := &Router{
	// 	trees: map[string]*Node{
	// 		http.MethodGet: root,
	// 	},
	// }

	cases := []struct {
		method  string
		path    string
		founded bool
		node    *Node
	}{
		{
			method:  http.MethodGet,
			path:    "/abc",
			founded: true,
			node:    rootWildCardNode,
		},
		{
			method:  http.MethodGet,
			path:    "/xyz/abc",
			founded: true,
			node:    rootSecondWildCardNode,
		},
		{
			method:  http.MethodGet,
			path:    "/xyz/space",
			founded: true,
			node:    spaceNode,
		},
		{
			method:  http.MethodGet,
			path:    "/user/abc",
			founded: true,
			node:    userWildCard,
		},
		{
			method:  http.MethodGet,
			path:    "/xyz/abc/home",
			founded: true,
			node:    homeNode,
		},
		{
			method:  http.MethodGet,
			path:    "/user/a",
			founded: true,
			node:    userWildCard,
		},
		{
			method:  http.MethodGet,
			path:    "/user/a/b",
			founded: true,
			node:    userWildCard,
		},
		{
			method:  http.MethodGet,
			path:    "/user/a/b/c",
			founded: true,
			node:    userWildCard,
		},
		{
			method:  http.MethodGet,
			path:    "/x/a/space",
			founded: true,
			node:    rootSecondWildCardNode,
		},
	}

	for _, tc := range cases {
		t.Run(tc.path, func(t *testing.T) {
			info, ok := router.FindRoute(tc.method, tc.path)
			if ok != tc.founded {
				t.Fatalf("path %s %q shoud %v but %v", tc.method, tc.path, tc.founded, ok)
			}
			if tc.founded {
				if msg, equal := tc.node.equalTo(info.node); !equal {
					t.Error(msg)
				}
			}
		})
	}
}

func TestFind_Param(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}

	router := NewRoute()
	router.AddRoute(http.MethodGet, "/", mockHandler)
	router.AddRoute(http.MethodGet, "/list", mockHandler)
	router.AddRoute(http.MethodGet, "/list/:catagory", mockHandler)

	catagoryNode := &Node{
		path:    ":catagory",
		handler: mockHandler,
	}

	listNode := &Node{
		path:    "list",
		handler: mockHandler,
		param:   catagoryNode,
	}

	root := &Node{
		path: "/",
		children: map[string]*Node{
			"list": listNode,
		},
		handler: mockHandler,
	}

	cases := []struct {
		method  string
		path    string
		founded bool
		node    *Node
	}{
		{
			method:  http.MethodGet,
			path:    "/",
			founded: true,
			node:    root,
		},
		{
			method:  http.MethodGet,
			path:    "/list",
			founded: true,
			node:    listNode,
		},
		{
			method:  http.MethodGet,
			path:    "/list/light",
			founded: true,
			node:    catagoryNode,
		},
	}

	for _, tc := range cases {
		t.Run(tc.path, func(t *testing.T) {
			info, ok := router.FindRoute(tc.method, tc.path)
			if ok != tc.founded {
				t.Fatalf("path %s %q shoud %v but %v", tc.method, tc.path, tc.founded, ok)
			}
			if tc.founded {
				if msg, equal := tc.node.equalTo(info.node); !equal {
					t.Error(msg)
				}
			}
		})
	}
}
func TestFind_RegEx(t *testing.T) {
	var mockHandler HandleFunc = func(ctx *Context) {}

	router := NewRoute()
	router.AddRoute(http.MethodGet, "/", mockHandler)
	router.AddRoute(http.MethodGet, "/user", mockHandler)
	router.AddRoute(http.MethodGet, "/user/:id(^[0-9]+$)", mockHandler)

	numReg, _ := regexp.Compile("^[0-9]+$")

	userRegExpNode := &Node{
		path:    ":id(^[0-9]+$)",
		handler: mockHandler,
	}

	userNode := &Node{
		path:    "user",
		handler: mockHandler,
		regmod:  numReg,
		regexp:  userRegExpNode,
	}

	cases := []struct {
		method  string
		path    string
		founded bool
		node    *Node
	}{
		{
			method:  http.MethodGet,
			path:    "/user",
			founded: true,
			node:    userNode,
		},
		{
			method:  http.MethodGet,
			path:    "/user/001",
			founded: true,
			node:    userRegExpNode,
		},
		{
			method:  http.MethodGet,
			path:    "/user/1123user",
			founded: false,
			node:    nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.path, func(t *testing.T) {
			info, ok := router.FindRoute(tc.method, tc.path)
			if ok != tc.founded {
				t.Fatalf("path %s %q shoud %v but %v", tc.method, tc.path, tc.founded, ok)
			}
			if tc.founded {
				if msg, equal := tc.node.equalTo(info.node); !equal {
					t.Error(msg)
				}
			}
		})
	}
}
func TestPanic_RegistWrongFormat(t *testing.T) {
	cases := []string{
		"",
		"user/home",
		"/user/home/",
		"//user/home",
		"/:",
		"/:...",
		"/:abc=",
		"/:abc(",
		"/:abc)(",
		"/*abc",
		"/*/**",
		"/**/*",
		"/:name(^^[^^^^^^^.....?????)",
	}
	for _, tc := range cases {
		t.Run(tc, func(t *testing.T) {
			assert.Panics(t, func() {
				router := NewRoute()
				router.AddRoute(http.MethodGet, tc, nil)
			})
		})

	}
}

func TestPanic_RegistRepeatRoute(t *testing.T) {
	cases := []struct {
		before string
		after  string
	}{
		{"/user/home", "/user/home"},
		{"/", "/"},
		{"/:id", "/:id"},
		{"/*", "/*"},
		{"/:id(^[0-9]+$)", "/:id(^[0-9]+$)"},
	}
	for _, tc := range cases {
		t.Run(fmt.Sprintf("repeat register %s should panic", tc.before), func(t *testing.T) {
			router := NewRoute()
			router.AddRoute(http.MethodGet, tc.before, func(ctx *Context) {})
			assert.Panics(t, func() {
				router.AddRoute(http.MethodGet, tc.after, func(ctx *Context) {})
			})
		})
	}
}

func TestPanic_RegistExclusiveRoute(t *testing.T) {
	cases := []struct {
		before string
		after  string
	}{
		{"/*", "/:page"},
		{"/:page(^[0-9]+$)", "/:page"},
		{"/:page", "/*"},
		{"/:page(^[0-9]+$)", "/*"},
		{"/:page", "/:number(^[0-9]+$)"},
		{"/*", "/:number(^[0-9]+$)"},
		{"/:page(^[0-9]+$)", "/:number(^[0-9]+$)"},
	}
	var mockHandler HandleFunc = func(ctx *Context) {}
	for _, tc := range cases {
		t.Run(fmt.Sprintf("need panic register %s after %s", tc.after, tc.before), func(t *testing.T) {
			router := NewRoute()
			router.AddRoute(http.MethodGet, tc.before, mockHandler)
			assert.Panics(t, func() {
				router.AddRoute(http.MethodGet, tc.after, mockHandler)
			})
		})
	}
}

func TestExtractMatchInfo_Param(t *testing.T) {
	router := NewRoute()
	var mockHandler HandleFunc = func(ctx *Context) {}
	router.AddRoute(http.MethodGet, "/page", mockHandler)
	router.AddRoute(http.MethodGet, "/page/:number", mockHandler)
	router.AddRoute(http.MethodGet, "/page/:number/start/:from", mockHandler)
	startNode := &Node{
		path: "start",
		param: &Node{
			path:    ":from",
			handler: mockHandler,
		},
	}
	node := &Node{
		path: ":number",
		children: map[string]*Node{
			"start": startNode,
		},
		handler: mockHandler,
	}
	pageNode := &Node{
		path:    "page",
		param:   node,
		handler: mockHandler,
	}

	cases := []struct {
		path      string
		wantNode  *Node
		wantParam map[string]string
	}{
		{
			path:      "/page/12",
			wantNode:  node,
			wantParam: map[string]string{"number": "12"},
		},
		{
			path:      "/page/12/start/July",
			wantNode:  startNode.param,
			wantParam: map[string]string{"number": "12", "from": "July"},
		},
		{
			path:      "/page",
			wantNode:  pageNode,
			wantParam: map[string]string{},
		},
	}

	for _, tc := range cases {
		t.Run(tc.path, func(t *testing.T) {
			info, ok := router.FindRoute(http.MethodGet, tc.path)

			assert.True(t, ok)
			assert.NotNil(t, info)
			assert.NotNil(t, info.node)

			if msg, equal := info.node.equalTo(tc.wantNode); !equal {
				t.Fatal(msg)
			}

			assert.Equal(t, tc.wantParam, info.param)
		})
	}
}

func TestExtractMatchInfo_RegEx(t *testing.T) {
	router := NewRoute()
	var mockHandler HandleFunc = func(ctx *Context) {}
	router.AddRoute(http.MethodGet, "/page", mockHandler)
	router.AddRoute(http.MethodGet, "/page/:number(^[0-9]+$)", mockHandler)
	router.AddRoute(http.MethodGet, "/page/:number(^[0-9]+$)/start/:from(^[0-9]+$)", mockHandler)

	numReg, _ := regexp.Compile("^[0-9]+$")

	fromRegNode := &Node{
		path:    ":from(^[0-9]+$)",
		handler: mockHandler,
	}

	startNode := &Node{
		path:   "start",
		regmod: numReg,
		regexp: fromRegNode,
	}

	numberRegNode := &Node{
		path: ":number(^[0-9]+$)",
		children: map[string]*Node{
			"start": startNode,
		},
		handler: mockHandler,
	}

	pageNode := &Node{
		path:    "page",
		regmod:  numReg,
		regexp:  numberRegNode,
		handler: mockHandler,
	}

	cases := []struct {
		path      string
		wantNode  *Node
		wantParam map[string]string
	}{
		{
			path:      "/page/12",
			wantNode:  numberRegNode,
			wantParam: map[string]string{"number": "12"},
		},
		{
			path:      "/page/12/start/001",
			wantNode:  fromRegNode,
			wantParam: map[string]string{"number": "12", "from": "001"},
		},
		{
			path:      "/page",
			wantNode:  pageNode,
			wantParam: map[string]string{},
		},
	}

	for _, tc := range cases {
		t.Run(tc.path, func(t *testing.T) {
			info, ok := router.FindRoute(http.MethodGet, tc.path)

			assert.True(t, ok)
			assert.NotNil(t, info)
			assert.NotNil(t, info.node)

			if msg, equal := info.node.equalTo(tc.wantNode); !equal {
				t.Fatal(msg)
			}

			assert.Equal(t, tc.wantParam, info.param)
		})
	}
}

func TestRegister_Middleware(t *testing.T) {
	m_1_count := 0
	var m_1 Middleware = func(hf HandleFunc) HandleFunc {
		return func(ctx *Context) { hf(ctx); m_1_count++ }
	}
	m_2_count := 0
	var m_2 Middleware = func(hf HandleFunc) HandleFunc {
		return func(ctx *Context) { hf(ctx); m_2_count++ }
	}
	t.Run("/a/b", func(t *testing.T) {
		trees := map[string]*Node{
			http.MethodGet: {
				path: "/",
				children: map[string]*Node{
					"a": {
						path: "a",
						children: map[string]*Node{
							"b": {
								path: "b",
								// mdls: []Middleware{m_1},
							},
						},
					},
				},
			},
		}
		want := &Node{
			path: "/",
			children: map[string]*Node{
				"a": {
					path: "a",
					children: map[string]*Node{
						"b": {
							path: "b",
							mdls: []Middleware{m_1},
						},
					},
				},
			},
		}
		router := &Router{trees: trees}
		router.AddRouteMiddleware(http.MethodGet, "/a/b", m_1)
		got := trees[http.MethodGet]
		if msg, equal := want.equalTo(got); !equal {
			t.Error(msg)
		}
	})

	t.Run("/a/*  and  /a/b/*", func(t *testing.T) {
		trees := map[string]*Node{
			http.MethodGet: {
				path: "/",
				children: map[string]*Node{
					"a": {
						path: "a",
						children: map[string]*Node{
							"b": {
								path: "b",
								wildcard: &Node{
									path: "*",
									// mdls: []Middleware{m_2},
								},
							},
						},
						wildcard: &Node{
							path: "*",
							// mdls: []Middleware{m_1},
						},
					},
				},
			},
		}
		want := &Node{
			path: "/",
			children: map[string]*Node{
				"a": {
					path: "a",
					children: map[string]*Node{
						"b": {
							path: "b",
							wildcard: &Node{
								path: "*",
								mdls: []Middleware{m_2},
							},
						},
					},
					wildcard: &Node{
						path: "*",
						mdls: []Middleware{m_1},
					},
				},
			},
		}
		router := &Router{trees: trees}

		router.AddRouteMiddleware(http.MethodGet, "/a/*", m_1)
		router.AddRouteMiddleware(http.MethodGet, "/a/b/*", m_2)

		if msg, equal := want.equalTo(router.trees[http.MethodGet]); !equal {
			t.Error(msg)
		}
	})
}
func TestFind_Middleware(t *testing.T) {
	m_1_count := 0
	var m_1 Middleware = func(hf HandleFunc) HandleFunc {
		return func(ctx *Context) { hf(ctx); m_1_count++ }
	}
	m_2_count := 0
	var m_2 Middleware = func(hf HandleFunc) HandleFunc {
		return func(ctx *Context) { hf(ctx); m_2_count++ }
	}
	t.Run("/a/b", func(t *testing.T) {
		trees := map[string]*Node{
			http.MethodGet: {
				path: "/",
				children: map[string]*Node{
					"a": {
						path: "a",
						children: map[string]*Node{
							"b": {
								path: "b",
								mdls: []Middleware{m_1},
							},
						},
					},
				},
			},
		}
		router := &Router{trees: trees}

		r_1 := router.FindMdls(trees[http.MethodGet], []string{"a", "b"})
		assertMiddlewaresEqual(t, []Middleware{m_1}, r_1)
		r_2 := router.FindMdls(trees[http.MethodGet], []string{"a", "b", "c"})
		assertMiddlewaresEqual(t, []Middleware{m_1}, r_2)
	})

	t.Run("/a/*  and  /a/b/*", func(t *testing.T) {
		trees := map[string]*Node{
			http.MethodGet: {
				path: "/",
				children: map[string]*Node{
					"a": {
						path: "a",
						children: map[string]*Node{
							"b": {
								path: "b",
								wildcard: &Node{
									path: "*",
									mdls: []Middleware{m_2},
								},
							},
						},
						wildcard: &Node{
							path: "*",
							mdls: []Middleware{m_1},
						},
					},
				},
			},
		}
		router := &Router{trees: trees}

		r_1 := router.FindMdls(trees[http.MethodGet], []string{"a", "c"})
		assertMiddlewaresEqual(t, []Middleware{m_1}, r_1)
		r_2 := router.FindMdls(trees[http.MethodGet], []string{"a", "b", "c"})
		assertMiddlewaresEqual(t, []Middleware{m_1, m_2}, r_2)
	})
	t.Run("/a/*/c  and  /a/b/c", func(t *testing.T) {
		trees := map[string]*Node{
			http.MethodGet: {
				path: "/",
				children: map[string]*Node{
					"a": {
						path: "a",
						children: map[string]*Node{
							"b": {
								path: "b",
								children: map[string]*Node{
									"c": {
										path: "c",
										mdls: []Middleware{m_2},
									},
								},
							},
						},
						wildcard: &Node{
							path: "*",
							children: map[string]*Node{
								"c": {
									path: "*",
									mdls: []Middleware{m_1},
								},
							},
						},
					},
				},
			},
		}
		router := &Router{trees: trees}

		r_1 := router.FindMdls(trees[http.MethodGet], []string{"a", "d", "c"})
		assertMiddlewaresEqual(t, []Middleware{m_1}, r_1)
		r_2 := router.FindMdls(trees[http.MethodGet], []string{"a", "b", "c"})
		assertMiddlewaresEqual(t, []Middleware{m_1, m_2}, r_2)
		r_3 := router.FindMdls(trees[http.MethodGet], []string{"a", "b", "d"})
		assertMiddlewaresEqual(t, []Middleware{}, r_3)
	})
	t.Run("/a/:id  and  /a/123/c", func(t *testing.T) {
		trees := map[string]*Node{
			http.MethodGet: {
				path: "/",
				children: map[string]*Node{
					"a": {
						path: "a",
						children: map[string]*Node{
							"123": {
								path: "123",
								children: map[string]*Node{
									"c": {
										path: "c",
										mdls: []Middleware{m_2},
									},
								},
							},
						},
						param: &Node{
							path: ":id",
							mdls: []Middleware{m_1},
						},
					},
				},
			},
		}
		router := &Router{trees: trees}

		r_1 := router.FindMdls(trees[http.MethodGet], []string{"a", "123"})
		assertMiddlewaresEqual(t, []Middleware{m_1}, r_1)
		r_2 := router.FindMdls(trees[http.MethodGet], []string{"a", "123", "c"})
		assertMiddlewaresEqual(t, []Middleware{m_1, m_2}, r_2)
	})
}

func assertMiddlewaresEqual(t *testing.T, want, got []Middleware) {
	t.Helper()
	if want == nil {
		if got != nil {
			t.Errorf("want nil got something len of got: %d", len(got))
		}
		return
	}

	if len(want) != len(got) {
		t.Errorf("want has %d middlewares but got %d of it", len(want), len(got))
	}

	for i, m := range want {
		fw := reflect.ValueOf(m)
		fg := reflect.ValueOf(got[i])
		if fw != fg {
			t.Errorf("the %dth middleware is not the same", i)
		}
	}
}
func BenchmarkFind_Static(b *testing.B) {
	router := getBenchMarkRouter()
	path := []string{"/", "/user", "/article", "/blog"}
	for i := 0; i < b.N; i++ {
		router.FindRoute(http.MethodGet, path[i%len(path)])
	}
}
func BenchmarkFind_Wildcard(b *testing.B) {
	router := getBenchMarkRouter()

	prefix := "/article/category"
	path := []string{"/a", "/b", "/c", "/d"}
	for i := 0; i < len(path); i++ {
		path[i] = prefix + path[i]
	}

	for i := 0; i < b.N; i++ {
		router.FindRoute(http.MethodGet, path[i%len(path)])
	}
}

func BenchmarkFind_Param(b *testing.B) {
	router := getBenchMarkRouter()

	prefix := "/blog"
	path := []string{"/a", "/b", "/c", "/d"}
	for i := 0; i < len(path); i++ {
		path[i] = prefix + path[i]
	}

	for i := 0; i < b.N; i++ {
		router.FindRoute(http.MethodGet, path[i%len(path)])
	}
}
func BenchmarkFind_RegEx(b *testing.B) {
	router := getBenchMarkRouter()

	prefix := "/user"
	path := make([]string, 100)
	for i := 0; i < len(path); i++ {
		num := rand.Intn(1000000)
		path[i] = prefix + "/" + strconv.Itoa(num)
	}

	for i := 0; i < b.N; i++ {
		router.FindRoute(http.MethodGet, path[i%len(path)])
	}
}
