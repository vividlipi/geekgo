package web

import (
	"fmt"
	"net"
	"net/http"
)

type HandleFunc func(ctx *Context)

type Server interface {
	http.Handler
	Start(string) error
	AddRoute(string, string, HandleFunc, ...Middleware)
}

type HTTPServerOption func(server *HTTPServer)

var _ Server = &HTTPServer{}

type HTTPServer struct {
	*Router
	mdls         []Middleware
	log          func(msg string, args ...any)
	RenderEngine RenderEngine
}

func NewHTTPServer(opts ...HTTPServerOption) *HTTPServer {
	server := &HTTPServer{
		Router: NewRoute(),
		log: func(msg string, args ...any) {
			fmt.Printf(msg, args...)
		},
	}
	for _, opt := range opts {
		opt(server)
	}
	return server
}

func ServerWithRenderEngine(engine RenderEngine) HTTPServerOption {
	return func(server *HTTPServer) {
		server.RenderEngine = engine
	}
}

func ServerWithMiddleware(mdls ...Middleware) HTTPServerOption {
	return func(server *HTTPServer) {
		server.mdls = mdls
	}
}

// ServeHTTP 处理请求的入口
func (h *HTTPServer) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	ctx := &Context{
		Req:          req,
		Resp:         resp,
		RenderEngine: h.RenderEngine,
	}
	root := h.serve
	for i := len(h.mdls) - 1; i >= 0; i-- {
		root = h.mdls[i](root)
	}
	var flush Middleware = func(hf HandleFunc) HandleFunc {
		return func(ctx *Context) {
			hf(ctx)
			h.flushResp(ctx)
		}
	}
	root = flush(root)
	root(ctx)
}

func (h *HTTPServer) flushResp(ctx *Context) {
	if ctx.RespStatusCode != 0 {
		ctx.Resp.WriteHeader(ctx.RespStatusCode)
	}
	_, err := ctx.Resp.Write(ctx.RespData)
	if err != nil {
		h.log("write content fail", err)
	}
}

func (h *HTTPServer) serve(ctx *Context) {
	// 查找路由，启动执行业务逻辑的方法
	info, found := h.FindRoute(ctx.Req.Method, ctx.Req.URL.Path)
	if !found || info.node.handler == nil {
		ctx.RespStatusCode = http.StatusNotFound
		ctx.RespData = []byte("Not Found")
		return
	}
	ctx.param = info.param
	ctx.MatchedRoute = info.node.route

	root := info.node.handler
	for i := len(info.mdls) - 1; i >= 0; i-- {
		root = info.mdls[i](root)
	}
	root(ctx)
}

func (h *HTTPServer) Get(path string, handler HandleFunc) {
	h.AddRoute(http.MethodGet, path, handler)
}

func (h *HTTPServer) Post(path string, handler HandleFunc) {
	h.AddRoute(http.MethodPost, path, handler)
}

func (h *HTTPServer) Start(addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	// 启动阶段注入其他逻辑
	return http.Serve(l, h)
}

func (h *HTTPServer) Use(method string, pattern string, mdls ...Middleware) {
	h.AddRouteMiddleware(method, pattern, mdls...)
}
