package web_test

import (
	"embed"
	"fmt"
	"geekgo/web"
	"html/template"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestServer(t *testing.T) {

	h := web.NewHTTPServer()
	h.Get("/user", func(ctx *web.Context) {
		ctx.Resp.WriteHeader(http.StatusOK)
		ctx.Resp.Write([]byte("hello"))
	})

	h.Get("/user/*", func(ctx *web.Context) {
		ctx.Resp.WriteHeader(http.StatusOK)
		content := fmt.Sprintf("URL: %s", ctx.Req.URL.String())
		ctx.Resp.Write([]byte(content))
	})

	h.Get("/*/*/home", func(ctx *web.Context) {
		ctx.Resp.WriteHeader(http.StatusOK)
		content := fmt.Sprintf("Home URL: %s", ctx.Req.URL.String())
		ctx.Resp.Write([]byte(content))
	})

	h.Get("/list/:category", func(ctx *web.Context) {
		ctx.Resp.WriteHeader(http.StatusOK)
		content := fmt.Sprintf("List Category URL: %s", ctx.Req.URL.String())
		ctx.Resp.Write([]byte(content))
	})

	h.Get("/show/:category", func(ctx *web.Context) {
		ctx.Resp.WriteHeader(http.StatusOK)
		category, _ := ctx.PathValue("category").ToString()
		content := fmt.Sprintf("Show Category: %s", category)
		ctx.Resp.Write([]byte(content))
	})

	t.Run("/user should be ok", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/user", nil)
		res := httptest.NewRecorder()
		want := "hello"
		h.ServeHTTP(res, req)

		assert.Equal(t, http.StatusOK, res.Result().StatusCode)

		if res.Body.String() != want {
			t.Errorf("got %q want %q", res.Body.String(), want)
		}
	})

	t.Run("/ should be not found", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		res := httptest.NewRecorder()
		want := "Not Found"
		h.ServeHTTP(res, req)

		assert.Equal(t, http.StatusNotFound, res.Result().StatusCode)

		if res.Body.String() != want {
			t.Errorf("got %q want %q", res.Body.String(), want)
		}
	})

	t.Run("wildcard", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/user/age", nil)
		res := httptest.NewRecorder()
		want := "URL: /user/age"
		h.ServeHTTP(res, req)

		if res.Body.String() != want {
			t.Errorf("got %q want %q", res.Body.String(), want)
		}
	})

	t.Run("wildcard home", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/luke/tom/home", nil)
		res := httptest.NewRecorder()
		want := "Home URL: /luke/tom/home"
		h.ServeHTTP(res, req)

		if res.Body.String() != want {
			t.Errorf("got %q want %q", res.Body.String(), want)
		}
	})

	t.Run("wildcard home", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/luke/tom", nil)
		res := httptest.NewRecorder()
		want := "Not Found"
		h.ServeHTTP(res, req)

		assert.Equal(t, http.StatusNotFound, res.Result().StatusCode)

		if res.Body.String() != want {
			t.Errorf("got %q want %q", res.Body.String(), want)
		}
	})

	t.Run("param list catagory", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/list/book", nil)
		res := httptest.NewRecorder()
		want := "List Category URL: /list/book"
		h.ServeHTTP(res, req)

		assert.Equal(t, http.StatusOK, res.Result().StatusCode)

		if res.Body.String() != want {
			t.Errorf("got %q want %q", res.Body.String(), want)
		}
	})

	t.Run("show catagory", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, "/show/book", nil)
		res := httptest.NewRecorder()
		want := "Show Category: book"
		h.ServeHTTP(res, req)

		assert.Equal(t, http.StatusOK, res.Result().StatusCode)

		if res.Body.String() != want {
			t.Errorf("got %q want %q", res.Body.String(), want)
		}
	})
}

func TestExtractPathParam(t *testing.T) {
	t.Run("get page number from query", func(t *testing.T) {
		h := web.NewHTTPServer()
		var page int64
		var err error
		h.Get("/list/:page", func(ctx *web.Context) {
			page, err = ctx.PathValue("page").ToInt64()
		})
		req := httptest.NewRequest(http.MethodGet, "/list/123", nil)
		res := httptest.NewRecorder()
		h.ServeHTTP(res, req)
		assert.Nil(t, err)
		assert.Equal(t, int64(123), page)
	})

	t.Run("get salary from form", func(t *testing.T) {
		form := url.Values{}
		form.Add("salary", "18888.909")
		form.Add("age", "18")

		h := web.NewHTTPServer()

		var salary float64
		var err error
		h.Post("/form", func(ctx *web.Context) {
			salary, err = ctx.FormValue("salary").ToFloat64()
		})

		req, _ := http.NewRequest(http.MethodPost, "/form", strings.NewReader(form.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		res := httptest.NewRecorder()

		h.ServeHTTP(res, req)

		assert.Nil(t, err)
		assert.Equal(t, 18888.909, salary)
	})
}

func TestMidware_Route(t *testing.T) {
	s := web.NewHTTPServer()
	s.Get("/a/b", func(ctx *web.Context) {})
	s.Get("/a/b/*", func(ctx *web.Context) {})

	access := 0
	var m web.Middleware = func(hf web.HandleFunc) web.HandleFunc {
		return func(ctx *web.Context) {
			hf(ctx)
			access++
		}
	}

	s.Use(http.MethodGet, "/a/b", m)

	t.Run("GET /a/b will execute midddleware", func(t *testing.T) {
		access = 0
		req := httptest.NewRequest(http.MethodGet, "/a/b", nil)
		res := httptest.NewRecorder()

		s.ServeHTTP(res, req)
		assert.Equal(t, 1, access)
	})
	t.Run("GET /a/b/c will execute midddleware", func(t *testing.T) {
		access = 0
		req := httptest.NewRequest(http.MethodGet, "/a/b/c", nil)
		res := httptest.NewRecorder()

		s.ServeHTTP(res, req)
		assert.Equal(t, 1, access)
	})
}

//go:embed "templates/*"
var Template embed.FS

func TestServer_Render(t *testing.T) {

	// using the function
	mydir, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(mydir)

	render, err := template.ParseFS(Template, "templates/*.gohtml")
	if err != nil {
		panic("template parse Error")
	}

	s := web.NewHTTPServer(web.ServerWithRenderEngine(&web.GoRenderEngine{Template: render}))
	s.Get("/space", func(ctx *web.Context) {
		if buf, err := ctx.RenderEngine.Render(ctx.Req.Context(), "space.gohtml", struct {
			Name string
		}{
			Name: "Lukeaxu",
		}); err == nil {
			ctx.RespStatusCode = http.StatusOK
			ctx.RespData = buf
		}
		ctx.RespStatusCode = http.StatusInternalServerError
	})

	req := httptest.NewRequest(http.MethodGet, "/space", nil)
	res := httptest.NewRecorder()
	s.ServeHTTP(res, req)

	want := `<p>hello, Lukeaxu!</p>`
	if res.Body.String() != want {
		t.Errorf("got %s want %s", res.Body.String(), want)
	}
}
