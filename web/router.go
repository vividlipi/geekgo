package web

import (
	"fmt"
	"regexp"
	"strings"
)

// Node 路由树中的节点，保存注册的路由节点
type Node struct {
	// 当前节点的完整路由
	route string

	// 当前节点的匹配规则
	path string

	// 静态匹配子节点
	children map[string]*Node

	// 参数路径匹配子节点
	param *Node

	// 通配符匹配子节点
	wildcard *Node

	// 正则匹配（路径中务必包含参数）子节点
	regmod *regexp.Regexp
	regexp *Node

	handler HandleFunc

	// 注册到路由上的 Middlewares
	mdls []Middleware
}

// Router 路由森林
type Router struct {
	trees map[string]*Node
}

// MatchInfo 路由查找结果
type MatchInfo struct {
	node  *Node
	param map[string]string

	// 注册到路由上的 Middlewares
	mdls []Middleware
}

// NewRoute 初始化路由森林
func NewRoute() *Router {
	return &Router{
		trees: make(map[string]*Node),
	}
}

func (r *Router) AddRoute(method string, path string, handler HandleFunc, mdls ...Middleware) {
	checkPathFormat(path)

	// Init Method Node in trees
	if r.trees == nil {
		r.trees = make(map[string]*Node)
	}

	if _, ok := r.trees[method]; !ok {
		r.trees[method] = &Node{
			path: "/",
		}
	}

	root := r.trees[method]

	segments := strings.Split(path[1:], "/")

	// 特殊处理：注册的path为"/"时，segments为[""]
	if len(segments) == 1 && segments[0] == "" {
		segments = segments[1:]
	}

	for _, subpath := range segments {
		checkSubPathFormat(subpath)
		// 正则匹配 参数路径
		if strings.HasPrefix(subpath, ":") {
			regExp, isRegExp := isContainRegExp(subpath)
			if isRegExp {
				// 正则匹配
				if root.regmod == nil {
					assertNotConflict(root)
					root.regmod = regExp
					root.regexp = &Node{
						path: subpath,
					}
				} else {
					if subpath != root.regexp.path {
						panic(fmt.Sprintf("registerd 2 diff regexp: %s and %s(exist)", regExp.String(), root.regmod.String()))
					}
				}
				root = root.regexp
				continue
			} else {
				// 参数路径
				if root.param == nil {
					assertNotConflict(root)
					root.param = &Node{
						path: subpath,
					}
				}
				root = root.param
				continue
			}
		}
		// 通配符匹配
		if subpath == "*" {
			if root.wildcard == nil {
				assertNotConflict(root)
				root.wildcard = &Node{
					path: subpath,
				}
			}
			root = root.wildcard
			continue
		}
		// 静态匹配
		if root.children == nil {
			root.children = map[string]*Node{}
		}

		if _, ok := root.children[subpath]; !ok {
			root.children[subpath] = &Node{
				path: subpath,
			}
		}
		root = root.children[subpath]
	}

	if root.handler != nil && handler != nil {
		panic(fmt.Sprintf("path %q registered twice", path))
	}

	root.handler = handler
	root.route = path
	root.mdls = append(root.mdls, mdls...)
}

func (r *Router) AddRouteMiddleware(method string, path string, mdls ...Middleware) {
	checkPathFormat(path)

	// Init Method Node in trees
	if r.trees == nil {
		panic("trees not exist")
	}

	if _, ok := r.trees[method]; !ok {
		panic("method root not exist")
	}

	root := r.trees[method]
	segments := strings.Split(path[1:], "/")

	// 特殊处理：注册的path为"/"时，segments为[""]
	if len(segments) == 1 && segments[0] == "" {
		segments = segments[1:]
	}

	for _, subpath := range segments {
		checkSubPathFormat(subpath)
		// 正则匹配 参数路径
		if strings.HasPrefix(subpath, ":") {
			regExp, isRegExp := isContainRegExp(subpath)
			if isRegExp {
				// 正则匹配
				if root.regmod == nil {
					panic("reg not regist")
				} else {
					if subpath != root.regexp.path {
						panic(fmt.Sprintf("registerd 2 diff regexp: %s and %s(exist)", regExp.String(), root.regmod.String()))
					}
				}
				root = root.regexp
			} else {
				// 参数路径
				if root.param == nil {
					panic("param not regist")
				} else {
					if subpath != root.param.path {
						panic("not the path param registed not the same")
					}
				}
				root = root.param
			}
			continue
		}
		// 通配符匹配
		if subpath == "*" {
			if root.wildcard == nil {
				panic("wild card not regist")
			}
			root = root.wildcard
			continue
		}
		// 静态匹配
		if root.children == nil {
			panic("have no child")
		}

		if _, ok := root.children[subpath]; !ok {
			panic(fmt.Sprintf("have no child of %s", subpath))
		}
		root = root.children[subpath]
	}
	root.mdls = append(root.mdls, mdls...)
}
func (r *Router) FindRoute(method, path string) (*MatchInfo, bool) {
	node, ok := r.trees[method]
	if !ok {
		return nil, false
	}
	root := node
	segments := strings.Split(path[1:], "/")

	// 特殊处理：注册的path为"/"时，segments为[""]
	if len(segments) == 1 && segments[0] == "" {
		segments = segments[1:]
	}

	param := make(map[string]string)
	for _, seg := range segments {
		// 静态匹配
		node, ok := root.children[seg]
		if !ok {
			// 正则匹配
			if root.regmod != nil {
				if root.regmod.MatchString(seg) {
					root = root.regexp
					paramName := root.path[1:strings.Index(root.path, "(")]
					param[paramName] = seg
					continue
				}
			}
			// 参数路径匹配
			if root.param != nil {
				root = root.param
				param[root.path[1:]] = seg
				continue
			}
			// 通配符匹配
			if root.wildcard != nil {
				root = root.wildcard
				continue
			}
			// 如果都没有得到匹配，并且最后节点为通配符节点，则返回通配符节点
			if root.path == "*" {
				break
			}
			return nil, false
		}
		root = node
	}

	mdls := r.FindMdls(node, segments)
	return &MatchInfo{node: root, param: param, mdls: mdls}, true
}

func (r *Router) FindMdls(root *Node, segments []string) []Middleware {
	stack := []Middleware{}
	seg_idx := 0
	var queue Queue[*Node]
	queue.Put(root)
	layer_current := 1
	layer_next := 0
	// 当 queue 不空：
	for queue.Size() > 0 {
		// 从队列中取出一个节点，将该节点 node 中的 ms 压入栈
		layer_current--
		node, _ := queue.Pop()
		if node.mdls != nil {
			stack = append(stack, node.mdls...)
		}
		if seg_idx < len(segments) {
			// if node 有统配节点： 将节点放入队列
			if node.wildcard != nil {
				layer_next++
				queue.Put(node.wildcard)
			}
			// if node 有路径节点： 将节点放入队列
			if node.param != nil {
				layer_next++
				queue.Put(node.param)
			}
			// if node 有正则节点 && segemtns[seg_idx]符合正则规则： 将节点放入队列
			if node.regmod != nil && node.regmod.MatchString(segments[seg_idx]) {
				layer_next++
				queue.Put(node.regexp)
			}
			// if 孩子节点匹配： 将节点放入队列
			if value, ok := node.children[segments[seg_idx]]; ok {
				layer_next++
				queue.Put(value)
			}
		}
		if layer_current == 0 {
			layer_current = layer_next
			layer_next = 0
			seg_idx++
		}
	}
	return stack
}

// checkPathFormat 检查注册的路径是否格式上有错误
// 如果不满足以下条件，则会发生 panic
// - 不是空字符串
// - 须以 / 开头
// - 不能以 / 结尾
// - 不能包含连续的 /
func checkPathFormat(path string) {
	if strings.Trim(path, " ") == "" {
		panic(fmt.Sprintf("path %q should not be empty", path))
	}

	if !strings.HasPrefix(path, "/") {
		panic(fmt.Sprintf("path %q should start with /", path))
	}

	if strings.HasSuffix(path, "/") && path != "/" {
		panic(fmt.Sprintf("path %q should not end with /", path))
	}

	if strings.Contains(path, "//") {
		panic(fmt.Sprintf("path %q should not contain //", path))
	}
}

// checkSubPathFormat 检查注册的节点路径是否格式上有错误
// 如果不满足以下条件，则会发生 panic
// - 如果是静态匹配，不是空字符串，且只包含字符数字
// - 如果是通配符匹配，则只包含一个 *
// - 如果是路径参数匹配，以 : 开头并且后面只有字符和数字
// - 如果是正则匹配，1. 以 : 开头 2. 后面根参数名称(不能为空) 3. ()中为正则表达式
func checkSubPathFormat(path string) {
	// if strings.Trim(path, " ") == "" {
	// 	panic("node path should not be empty")
	// }

	if strings.HasPrefix(path, "*") {
		if len(path) != 1 {
			panic(fmt.Sprintf("node path %s should not have other character after *", path))
		}
	}

	if strings.HasPrefix(path, ":") {
		paramPathReg, _ := regexp.Compile("^:[a-zA-Z0-9]+$")
		if !paramPathReg.MatchString(path) {
			start := strings.Index(path, "(")
			if !paramPathReg.MatchString(path[:start]) {
				panic("param name should only contains a-zA-Z0-9")
			}
			_, contained := isContainRegExp(path[start:])
			if !contained {
				panic(fmt.Sprintf("node path has not valid regexp %s", path))
			}
		}
	}

}

// isContainRegExp 判断字符串中是否包含 (RegExp) 模式
func isContainRegExp(subpath string) (*regexp.Regexp, bool) {
	re := regexp.MustCompile(`\((.*?)\)`)
	match := re.FindStringSubmatch(subpath)
	if len(match) < 2 {
		return nil, false
	}
	reInPath, err := regexp.Compile(match[1])

	if err != nil {
		return nil, false
	}
	return reInPath, true
}

// assertNotConflict 建立新的（通配 || 参数路径 || 正则）节点前调用
// 确保不会同时注册多个非静态匹配节点
func assertNotConflict(node *Node) {
	if node.param != nil || node.wildcard != nil || node.regexp != nil {
		panic(fmt.Sprintf("register to %s conflit", node.path))
	}
}
