package concurrency_test

import (
	"fmt"
	"geekgo/concurrency"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestMQ(t *testing.T) {
	mq := &concurrency.Broker[int]{}
	wg := sync.WaitGroup{}
	// 每个 topic 有 5 个订阅者，一个有 10 个 topic
	for i := 0; i < 10; i++ {
		count := 5
		wg.Add(count)
		receiver := func(topic string) {
			defer wg.Done()
			msgs, err := concurrency.Subscribe(mq, topic, 100)
			require.NoError(t, err)
			for msg := range msgs {
				fmt.Println(topic, msg)
			}
			fmt.Println("Topic Done: ", topic)
		}
		for count > 0 {
			go receiver(fmt.Sprint(i))
			count--
		}
	}
	sendwg := sync.WaitGroup{}
	// 每个 topic 有 2 个发布者，每个发布者发布 10 条消息
	for i := 0; i < 10; i++ {
		count := 2
		sendwg.Add(count)
		sender := func(topic string) {
			defer sendwg.Done()
			for j := 0; j < 2; j++ {
				mq.Send(j, topic)
				time.Sleep(time.Millisecond * 50)
			}
		}
		for count > 0 {
			go sender(fmt.Sprint(i))
			count--
		}
	}
	sendwg.Wait()
	// 等待发布者发完消息后，关闭 topic
	for i := 0; i < 10; i++ {
		err := mq.CloseTopic(fmt.Sprint(i))
		require.NoError(t, err)
	}
	// 等待订阅者退出
	wg.Wait()
}
