package concurrency

import (
	"fmt"
	"sync"
)

type Broker[T any] struct {
	mutex  sync.RWMutex
	chans  map[string][]chan T
	once   sync.Once
	closed bool
}

func Subscribe[T any](broker *Broker[T], topic string, cap int) (<-chan T, error) {
	broker.mutex.Lock()
	defer broker.mutex.Unlock()

	if broker.closed {
		return nil, fmt.Errorf("broker closed")
	}

	if broker.chans == nil {
		broker.chans = make(map[string][]chan T)
	}
	channel := make(chan T, cap)
	broker.chans[topic] = append(broker.chans[topic], channel)
	return channel, nil
}

func (b *Broker[T]) Send(data T, topic string) error {
	b.mutex.RLock()
	defer b.mutex.RUnlock()

	channels, ok := b.chans[topic]
	if !ok {
		return fmt.Errorf("topic %s has no receiver", topic)
	}

	for i, channel := range channels {
		select {
		case channel <- data:
		default:
			fmt.Println("Send Failed After 100ms to channel: ", i)
		}
	}
	return nil
}

func (b *Broker[T]) CloseTopic(topic string) error {
	b.mutex.Lock()
	channels, ok := b.chans[topic]
	delete(b.chans, topic)
	b.mutex.Unlock()

	if !ok {
		return fmt.Errorf("topic: %s done not exist", topic)
	}

	for _, channel := range channels {
		close(channel)
	}
	return nil
}

func (b *Broker[T]) Close() error {
	b.once.Do(func() {
		b.mutex.Lock()
		defer b.mutex.Unlock()
		b.closed = true
		for _, channels := range b.chans {
			for _, channel := range channels {
				close(channel)
			}
		}
	})
	return nil
}
