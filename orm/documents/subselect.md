# 子查询

## 场景分析
### SQL语法角度
[Mysql](https://dev.mysql.com/doc/refman/8.0/en/subqueries.html)
[Sqlite](https://www.sqlite.org/lang_select.html)
子查询简单定义成：包含在其他SQL中的Select语句，可选别名，支持嵌套

子查询两种经常的使用：
1. 用在Select语句的From后面作为 TableReference 使用
2. 作为组成条件 Predicate 的一部分使用，例如 IN (sub_query), EXIST (sub_query)

### 用户角度

1. 查询中包含子查询
select * from (select * from table where x)

## 行业分析

### GORM子查询的支持
GORM


