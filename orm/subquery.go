package orm

import "geekgo/orm/model"

type SubQuery struct {
	s     QueryBuilder
	model *model.Model
	alias string
}

func (s SubQuery) C(col string) Column {
	return Column{
		table: s,
		name:  col,
	}
}
func (s SubQuery) As(alias string) SubQuery {
	return SubQuery{
		s:     s.s,
		model: s.model,
		alias: alias,
	}
}

func (s SubQuery) tableAlias() string {
	return s.alias
}
