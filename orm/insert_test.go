package orm

import (
	"context"
	"database/sql/driver"
	"errors"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type Teacher struct {
	Name   string
	Age    int
	Grade  int
	Course string
	Salary int
}

func TestInsert_Build(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testcases := []struct {
		name      string
		builder   QueryBuilder
		wantErr   error
		wantQuery *Query
	}{
		{
			name: "Insert into Teacher with one row",
			builder: NewInsertor[Teacher](db).Columns(C("Name"), C("Age")).Values(&Teacher{
				Name:   "Bob",
				Age:    44,
				Grade:  1,
				Course: "English",
				Salary: 5500,
			}),
			wantQuery: &Query{
				SQL:  "INSERT INTO `teacher` (`name`,`age`) VALUES (?,?);",
				Args: []any{"Bob", 44},
			},
		},
		{
			name: "Insert into Teacher with multiple rows",
			builder: NewInsertor[Teacher](db).Values(&Teacher{
				Name:   "Bob",
				Age:    44,
				Grade:  1,
				Course: "English",
				Salary: 5500,
			}, &Teacher{
				Name:   "Tom",
				Age:    66,
				Grade:  5,
				Course: "数学",
				Salary: 6800,
			}),
			wantQuery: &Query{
				SQL:  "INSERT INTO `teacher` (`name`,`age`,`grade`,`course`,`salary`) VALUES (?,?,?,?,?),(?,?,?,?,?);",
				Args: []any{"Bob", 44, 1, "English", 5500, "Tom", 66, 5, "数学", 6800},
			},
		},
		{
			name: "upsert Teacher with multiple rows",
			builder: NewInsertor[Teacher](db).Values(&Teacher{
				Name:   "Bob",
				Age:    44,
				Grade:  1,
				Course: "English",
				Salary: 5500,
			}, &Teacher{
				Name:   "Tom",
				Age:    66,
				Grade:  5,
				Course: "数学",
				Salary: 6800,
			}).OnDuplicateKey().Update(C("Grade"), C("Salary")),
			wantQuery: &Query{
				SQL: "INSERT INTO `teacher` (`name`,`age`,`grade`,`course`,`salary`) VALUES (?,?,?,?,?),(?,?,?,?,?) " +
					"ON DUPLICATE KEY UPDATE `grade`=VALUES(`grade`),`salary`=VALUES(`salary`);",
				Args: []any{"Bob", 44, 1, "English", 5500, "Tom", 66, 5, "数学", 6800},
			},
		},
		{
			name: "Insert into Teacher with one row",
			builder: NewInsertor[Teacher](db).Columns(C("Name"), C("Age")).Values(&Teacher{
				Name:   "Bob",
				Age:    44,
				Grade:  1,
				Course: "English",
				Salary: 5500,
			}).OnDuplicateKey().Update(Assign("Age", 44)),
			wantQuery: &Query{
				SQL:  "INSERT INTO `teacher` (`name`,`age`) VALUES (?,?) ON DUPLICATE KEY UPDATE `age`=?;",
				Args: []any{"Bob", 44, 44},
			},
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			r, err := tc.builder.Build()
			if tc.wantErr == nil {
				assert.Equal(t, tc.wantQuery, r)
			}
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func TestInsert_Sqlite3Dialect(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB, WithDialect(DialectSqlite3))
	testcases := []struct {
		name      string
		builder   QueryBuilder
		wantErr   error
		wantQuery *Query
	}{
		{
			name: "Insert into Teacher with one row",
			builder: NewInsertor[Teacher](db).Columns(C("Name"), C("Age")).Values(&Teacher{
				Name:   "Bob",
				Age:    44,
				Grade:  1,
				Course: "English",
				Salary: 5500,
			}).OnDuplicateKey().OnConflict(C("Name")).Update(Assign("Age", 44)),
			wantQuery: &Query{
				SQL:  "INSERT INTO `teacher` (`name`,`age`) VALUES (?,?) ON CONFLICT(`name`) DO UPDATE SET `age`=?;",
				Args: []any{"Bob", 44, 44},
			},
		},
		{
			name: "upsert Teacher with multiple rows",
			builder: func() QueryBuilder {
				return NewInsertor[Teacher](db).Values(&Teacher{
					Name:   "Bob",
					Age:    44,
					Grade:  1,
					Course: "English",
					Salary: 5500,
				}, &Teacher{
					Name:   "Tom",
					Age:    66,
					Grade:  5,
					Course: "数学",
					Salary: 6800,
				}).OnDuplicateKey().OnConflict(C("Grade")).Update(C("Grade"), C("Salary"))
			}(),
			wantQuery: &Query{
				SQL: "INSERT INTO `teacher` (`name`,`age`,`grade`,`course`,`salary`) VALUES (?,?,?,?,?),(?,?,?,?,?) " +
					"ON CONFLICT(`grade`) DO UPDATE SET `grade`=excluded.`grade`,`salary`=excluded.`salary`;",
				Args: []any{"Bob", 44, 1, "English", 5500, "Tom", 66, 5, "数学", 6800},
			},
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			r, err := tc.builder.Build()
			if tc.wantErr == nil {
				assert.Equal(t, tc.wantQuery, r)
			}
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func TestInserter_Exec(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	require.NoError(t, err)
	db, err := OpenDB(mockDB)
	require.NoError(t, err)
	testCases := []struct {
		name     string
		i        *Insertor[TestModel]
		wantErr  error
		affected int64
	}{
		// {
		// 	name: "query error",
		// 	i: func() *Insertor[TestModel] {
		// 		return NewInsertor[TestModel](db).Values(&TestModel{}).Columns(C("Invalid"))
		// 	}(),
		// 	wantErr: errs.NewErrUnknownField("Invalid"),
		// },
		{
			name: "db error",
			i: func() *Insertor[TestModel] {
				mock.ExpectExec("INSERT INTO .*").WillReturnError(errors.New("db error"))
				return NewInsertor[TestModel](db).Values(&TestModel{})
			}(),
			wantErr: errors.New("db error"),
		},
		{
			name: "exec",
			i: func() *Insertor[TestModel] {
				res := driver.RowsAffected(1)
				mock.ExpectExec("INSERT INTO .*").WillReturnResult(res)
				return NewInsertor[TestModel](db).Values(&TestModel{})
			}(),
			affected: 1,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := tc.i.Exec(context.Background())
			affected, err := res.RowsAffected()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.affected, affected)
		})
	}
}
