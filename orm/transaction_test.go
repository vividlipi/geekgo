package orm_test

type Person struct {
	Id   int
	Name string
	Age  int
}

// func Test_NewTransaction(t *testing.T) {
// 	mockDB, _, _ := sqlmock.New()
// 	db, _ := orm.OpenDB(mockDB)
// 	tx, _ := db.NewTx(context.Background(), nil)
// 	s, _ := orm.NewSelector[Person](tx)
// 	result, _ := s.Where(C("id").Eq(1)).Build().Exec()
// 	if len(result) == 0 {
// 		i, _ := orm.NewInsertor[Person](tx)
// 		i.Values(Person{Id: 1, Name: "tx", Age: 18}).Build().ExecContext()
// 	}
// 	tx.Commit()
// }
