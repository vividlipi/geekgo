package orm

import (
	"context"
	"database/sql"
)

var _ Session = &DB{}
var _ Session = &Tx{}

type Session interface {
	getCore() core
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
}
