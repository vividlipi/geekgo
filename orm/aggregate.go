package orm

type Aggregate struct {
	fn    string
	arg   string
	alias string
}

func (agg Aggregate) selectable() {}
func (agg Aggregate) expr()       {}

func (agg Aggregate) As(alias string) Aggregate {
	return Aggregate{fn: agg.fn, arg: agg.arg, alias: alias}
}

func Sum(col string) Aggregate {
	return Aggregate{fn: "SUM", arg: col}
}

func Max(col string) Aggregate {
	return Aggregate{fn: "MAX", arg: col}
}

func Min(col string) Aggregate {
	return Aggregate{fn: "MIN", arg: col}
}

func (agg Aggregate) Gt(val any) Predicate {
	return Predicate{left: agg, op: opGt, right: valueOf(val)}
}
