package orm_test

import (
	"geekgo/orm"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
)

func Test_DB(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	orm.OpenDB(mockDB)
}
