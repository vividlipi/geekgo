package orm

type Orderable interface {
	orderable()
}

type OrderSC struct {
	item Column
	asc  bool
}

func (o OrderSC) orderable() {}
