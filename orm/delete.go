package orm

type Deleter[T any] struct {
	builder
	table   string
	where   []Predicate
	core    core
	session Session
}

func NewDeleter[T any](session Session) *Deleter[T] {
	core := session.getCore()
	model, err := core.r.Get(new(T))
	if err != nil {
		panic("Failed to map Type to Model")
	}
	return &Deleter[T]{
		builder: builder{r: core.r, dialect: core.dialect, model: model},
		core:    core,
		session: session,
	}
}

func (d *Deleter[T]) Build() (*Query, error) {
	// From
	d.sb.WriteString("DELETE FROM ")
	if d.table == "" {
		d.writeWrap(d.GetTableName())
	} else {
		d.writeWrap(d.table)
	}

	// Where
	if len(d.where) > 0 {
		d.sb.WriteString(" WHERE ")
		if err := d.builder.Build(d.where...); err != nil {
			return nil, err
		}
	}
	d.sb.WriteByte(';')
	return &Query{SQL: d.sb.String(), Args: d.args}, nil
}

// From accepts model definition
func (d *Deleter[T]) From(table string) *Deleter[T] {
	d.table = table
	return d
}

// Where accepts predicates
func (d *Deleter[T]) Where(predicates ...Predicate) *Deleter[T] {
	d.where = predicates
	return d
}
