package model_test

import (
	"geekgo/orm/model"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

type User struct {
	Name string `orm:"column=name_column"`
	Age  int
}

func (u User) TableName() string {
	return "user"
}

func Test_RegistryModel(t *testing.T) {
	registry := model.NewRegistry()
	registry.Register(new(User), func(model *model.Model) error {
		model.TableName = "user_table"
		return nil
	})
	m, err := registry.Get(new(User))

	assert.Nil(t, err)
	assert.Equal(t, m.TableName, "user_table")
	assert.Equal(t, m.FieldMap["Name"], &model.Field{
		ColName: "name_column",
		Typ:     reflect.TypeOf(""),
		GoName:  "Name",
	})
	assert.Equal(t, m.ColumnMap["name_column"], &model.Field{
		ColName: "name_column",
		Typ:     reflect.TypeOf(""),
		GoName:  "Name",
	})
}
