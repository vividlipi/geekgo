package model

import "reflect"

type Model struct {
	TableName string
	Fields    []*Field
	FieldMap  map[string]*Field
	ColumnMap map[string]*Field
}

type Field struct {
	Index   int
	ColName string
	Typ     reflect.Type
	GoName  string
	Offset  uintptr
}

// TableName 用户实现这个接口来返回自定义的表名
type TableName interface {
	TableName() string
}
