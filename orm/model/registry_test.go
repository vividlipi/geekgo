package model

import (
	"database/sql"
	"errors"
	"reflect"
	"sync"
	"testing"
	"unsafe"

	"github.com/stretchr/testify/assert"
)

type TestModel struct {
	Id        int64
	FirstName string
	Age       int8
	LastName  *sql.NullString
}

func TestGetConcurencySupport(t *testing.T) {
	itemCont := 1000000
	entities := make([]any, itemCont)
	for i := range entities {
		entities[i] = &TestModel{}
	}

	var wg sync.WaitGroup
	wg.Add(itemCont)

	var lock sync.Mutex
	got := make(map[uintptr]int)

	registry := &registry{models: sync.Map{}}
	for i := 0; i < itemCont; i++ {
		entity := entities[i]
		go func() {
			m, _ := registry.Get(entity)
			// fatal error: concurrent map writes
			lock.Lock()
			got[uintptr(unsafe.Pointer(m))]++
			lock.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()

	assert.Equal(t, 1, GetLenOfSyncMap(&registry.models))
	// fmt.Printf("%v", got)
}

func TestGetUseCache(t *testing.T) {
	testCases := []struct {
		name          string
		val           []any
		wantItemCount int
	}{
		{
			name:          "only support pointer to struct",
			val:           []any{TestModel{}},
			wantItemCount: 0,
		},
		{
			name:          "get then cache",
			val:           []any{&TestModel{}},
			wantItemCount: 1,
		},
		{
			name:          "query from cache",
			val:           []any{&TestModel{}, &TestModel{}, &TestModel{}},
			wantItemCount: 1,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			registry := &registry{models: sync.Map{}}
			for _, entity := range tc.val {
				registry.Get(entity)
			}
			assert.Equal(t, tc.wantItemCount, GetLenOfSyncMap(&registry.models))
		})
	}
}

func TestParseModel(t *testing.T) {
	testCases := []struct {
		name      string
		val       any
		wantModel *Model
		wantErr   error
	}{
		{
			name:    "test model",
			val:     TestModel{},
			wantErr: errors.New("orm: 只支持一级指针作为输入，例如 *User"),
		},
		{
			// 指针
			name: "pointer",
			val:  &TestModel{},
			wantModel: &Model{
				TableName: "test_model",
				Fields: []*Field{
					{
						Index:   0,
						ColName: "id",
						Typ:     reflect.TypeOf(int64(0)),
						GoName:  "Id",
						Offset:  (uintptr)(0),
					},
					{
						Index:   1,
						ColName: "first_name",
						Typ:     reflect.TypeOf(""),
						GoName:  "FirstName",
						Offset:  (uintptr)(8),
					},
					{
						Index:   2,
						ColName: "age",
						Typ:     reflect.TypeOf(int8(0)),
						GoName:  "Age",
						Offset:  (uintptr)(24),
					},
					{
						Index:   3,
						ColName: "last_name",
						Typ:     reflect.TypeOf(&sql.NullString{}),
						GoName:  "LastName",
						Offset:  (uintptr)(32),
					},
				},
				FieldMap: map[string]*Field{
					"Id": {
						Index:   0,
						ColName: "id",
						Typ:     reflect.TypeOf(int64(0)),
						GoName:  "Id",
						Offset:  (uintptr)(0),
					},
					"FirstName": {
						Index:   1,
						ColName: "first_name",
						Typ:     reflect.TypeOf(""),
						GoName:  "FirstName",
						Offset:  (uintptr)(8),
					},
					"Age": {
						Index:   2,
						ColName: "age",
						Typ:     reflect.TypeOf(int8(0)),
						GoName:  "Age",
						Offset:  (uintptr)(24),
					},
					"LastName": {
						Index:   3,
						ColName: "last_name",
						Typ:     reflect.TypeOf(&sql.NullString{}),
						GoName:  "LastName",
						Offset:  (uintptr)(32),
					},
				},
				ColumnMap: map[string]*Field{
					"id": {
						Index:   0,
						ColName: "id",
						Typ:     reflect.TypeOf(int64(0)),
						GoName:  "Id",
						Offset:  (uintptr)(0),
					},
					"first_name": {
						Index:   1,
						ColName: "first_name",
						Typ:     reflect.TypeOf(""),
						GoName:  "FirstName",
						Offset:  (uintptr)(8),
					},
					"age": {
						Index:   2,
						ColName: "age",
						Typ:     reflect.TypeOf(int8(0)),
						GoName:  "Age",
						Offset:  (uintptr)(24),
					},
					"last_name": {
						Index:   3,
						ColName: "last_name",
						Typ:     reflect.TypeOf(&sql.NullString{}),
						GoName:  "LastName",
						Offset:  (uintptr)(32),
					},
				},
			},
		},
		{
			// 多级指针
			name: "multiple pointer",
			// 因为 Go 编译器的原因，所以我们写成这样
			val: func() any {
				val := &TestModel{}
				return &val
			}(),
			wantErr: errors.New("orm: 只支持一级指针作为输入，例如 *User"),
		},
		{
			name:    "map",
			val:     map[string]string{},
			wantErr: errors.New("orm: 只支持一级指针作为输入，例如 *User"),
		},
		{
			name:    "slice",
			val:     []int{},
			wantErr: errors.New("orm: 只支持一级指针作为输入，例如 *User"),
		},
		{
			name:    "basic type",
			val:     0,
			wantErr: errors.New("orm: 只支持一级指针作为输入，例如 *User"),
		},
	}

	registry := &registry{models: sync.Map{}}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			m, err := registry.parseModel(tc.val)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantModel, m)
		})
	}
}

func TestUnderscoreName(t *testing.T) {
	testCases := []struct {
		name    string
		srcStr  string
		wantStr string
	}{
		// 我们这些用例就是为了确保
		// 在忘记 underscoreName 的行为特性之后
		// 可以从这里找回来
		// 比如说过了一段时间之后
		// 忘记了 ID 不能转化为 id
		// 那么这个测试能帮我们确定 ID 只能转化为 i_d
		{
			name:    "upper cases",
			srcStr:  "ID",
			wantStr: "i_d",
		},
		{
			name:    "use number",
			srcStr:  "Table1Name",
			wantStr: "table1_name",
		},
	}

	registry := &registry{models: sync.Map{}}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := registry.underscoreName(tc.srcStr)
			assert.Equal(t, tc.wantStr, res)
		})
	}
}

func TestParseModelTagColumnName(t *testing.T) {
	testCases := []struct {
		name      string
		entity    any
		wantErr   error
		wantModel *Model
	}{
		{
			name: "Normal Use Case",
			entity: func() any {
				type One struct {
					Name string `orm:"column=name_t"`
				}
				return &One{}
			}(),
			wantModel: &Model{
				TableName: "one",
				Fields: []*Field{
					{
						ColName: "name_t",
						Typ:     reflect.TypeOf(""),
						GoName:  "Name",
					},
				},
				FieldMap: map[string]*Field{
					"Name": {
						ColName: "name_t",
						Typ:     reflect.TypeOf(""),
						GoName:  "Name",
					},
				},
				ColumnMap: map[string]*Field{
					"name_t": {
						ColName: "name_t",
						Typ:     reflect.TypeOf(""),
						GoName:  "Name",
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			registry := &registry{models: sync.Map{}}
			m, err := registry.parseModel(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantModel, m)
		})
	}
}

func TestParseModelImplTableName(t *testing.T) {
	testCases := []struct {
		name      string
		entity    any
		wantErr   error
		wantModel *Model
	}{
		{
			name:   "Normal Use Case",
			entity: &CustomTableNamePtr{},
			wantModel: &Model{
				TableName: "custom_table_name_ptr_t",
				Fields: []*Field{
					{
						ColName: "name",
						Typ:     reflect.TypeOf(""),
						GoName:  "Name",
					},
				},
				FieldMap: map[string]*Field{
					"Name": {
						ColName: "name",
						Typ:     reflect.TypeOf(""),
						GoName:  "Name",
					},
				},
				ColumnMap: map[string]*Field{
					"name": {
						ColName: "name",
						Typ:     reflect.TypeOf(""),
						GoName:  "Name",
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			registry := &registry{models: sync.Map{}}
			m, err := registry.parseModel(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantModel, m)
		})
	}
}

type CustomTableNamePtr struct {
	Name string
}

func (c *CustomTableNamePtr) TableName() string {
	return "custom_table_name_ptr_t"
}

func TestRegisterModel(t *testing.T) {
	registry := NewRegistry()
	want := &Model{
		TableName: "custom_t",
		Fields: []*Field{
			{
				ColName: "name_s",
				Typ:     reflect.TypeOf(""),
				GoName:  "Name",
			},
		},
		FieldMap: map[string]*Field{
			"Name": {
				ColName: "name_s",
				Typ:     reflect.TypeOf(""),
				GoName:  "Name",
			},
		},
		ColumnMap: map[string]*Field{
			"name_s": {
				ColName: "name_s",
				Typ:     reflect.TypeOf(""),
				GoName:  "Name",
			},
		},
	}
	m, err := registry.Register(&CustomTableNamePtr{}, WithTableName("custom_t"), WithColumnName("Name", "name_s"))
	assert.Equal(t, nil, err)
	assert.Equal(t, want, m)
}

func GetLenOfSyncMap(m *sync.Map) (count int) {
	// var count int
	m.Range(func(key, value interface{}) bool {
		count++
		return true
	})
	return
}
