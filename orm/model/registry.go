package model

import (
	"geekgo/orm/internal/errs"
	"reflect"
	"strings"
	"sync"
	"unicode"
)

const TagKey = "orm"
const KeyColumn = "column"

type ModelOpt func(model *Model) error

// Registry 元数据注册中心的抽象
type Registry interface {
	// Get 查找元数据
	Get(val any) (*Model, error)
	// Register 注册一个模型
	Register(val any, opts ...ModelOpt) (*Model, error)
}

type registry struct {
	models sync.Map
}

func NewRegistry() Registry {
	return &registry{
		models: sync.Map{},
	}
}

func (r *registry) Get(val any) (*Model, error) {
	typ := reflect.TypeOf(val)
	m, ok := r.models.Load(typ)
	if !ok {
		var err error
		if m, err = r.Register(val); err != nil {
			return nil, err
		}
		r.models.Store(typ, m)
	}
	return m.(*Model), nil
}

func (r *registry) Register(val any, opts ...ModelOpt) (*Model, error) {
	m, err := r.parseModel(val)
	if err != nil {
		return nil, err
	}

	for _, opt := range opts {
		err = opt(m)
		if err != nil {
			return nil, err
		}
	}

	typ := reflect.TypeOf(val)
	r.models.Store(typ, m)
	return m, nil
}

func (r *registry) parseModel(entity any) (*Model, error) {
	typ := reflect.TypeOf(entity)
	if typ.Kind() != reflect.Pointer || typ.Elem().Kind() != reflect.Struct {
		return nil, errs.ErrPointerOnly
	}
	typ = typ.Elem()
	numField := typ.NumField()
	fds := make(map[string]*Field, numField)
	cds := make(map[string]*Field, numField)
	fs := make([]*Field, numField)
	for i := 0; i < numField; i++ {

		fdType := typ.Field(i)
		tags, err := r.parseTag(fdType.Tag)
		if err != nil {
			return nil, err
		}
		colName := tags[KeyColumn]
		if colName == "" {
			colName = r.underscoreName(fdType.Name)
		}
		f := &Field{
			Index:   i,
			ColName: colName,
			Typ:     fdType.Type,
			GoName:  fdType.Name,
			Offset:  fdType.Offset,
		}
		fds[fdType.Name] = f
		cds[colName] = f
		fs[i] = f
	}

	var tableName string
	if tn, ok := entity.(TableName); ok {
		tableName = tn.TableName()
	}

	if tableName == "" {
		tableName = r.underscoreName(typ.Name())
	}

	return &Model{
		TableName: tableName,
		Fields:    fs,
		FieldMap:  fds,
		ColumnMap: cds,
	}, nil
}

func (r *registry) parseTag(tag reflect.StructTag) (map[string]string, error) {
	content := tag.Get(TagKey)
	if content == "" {
		// 返回一个空的 map，这样调用者就不需要判断 nil 了
		return map[string]string{}, nil
	}
	// 这个初始化容量就是我们支持的 key 的数量，
	// 现在只有一个，所以我们初始化为 1
	res := make(map[string]string, 1)

	// 接下来就是字符串处理了
	pairs := strings.Split(content, ",")
	for _, pair := range pairs {
		kv := strings.Split(pair, "=")
		if len(kv) != 2 {
			return nil, errs.NewErrInvalidTagContent(pair)
		}
		res[kv[0]] = kv[1]
	}
	return res, nil
}

// underscoreName 驼峰转字符串命名
func (r *registry) underscoreName(TableName string) string {
	var buf []byte
	for i, v := range TableName {
		if unicode.IsUpper(v) {
			if i != 0 {
				buf = append(buf, '_')
			}
			buf = append(buf, byte(unicode.ToLower(v)))
		} else {
			buf = append(buf, byte(v))
		}

	}
	return string(buf)
}

func WithTableName(tableName string) ModelOpt {
	return func(model *Model) error {
		model.TableName = tableName
		return nil
	}
}

func WithColumnName(field string, columnName string) ModelOpt {
	return func(model *Model) error {
		fd, ok := model.FieldMap[field]
		if !ok {
			return errs.NewErrUnknownField(field)
		}
		// 注意，这里我们根本没有检测 ColName 会不会是空字符串
		// 因为正常情况下，用户都不会写错
		// 即便写错了，也很容易在测试中发现
		delete(model.ColumnMap, fd.ColName)
		fd.ColName = columnName
		model.ColumnMap[columnName] = fd
		return nil
	}
}
