package orm

import (
	"geekgo/orm/internal/errs"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
)

// type TestModel struct {
// 	Id        int64
// 	FirstName string
// 	Age       int8
// 	LastName  *sql.NullString
// }

func Test_SubQueryInFrom(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testcases := []struct {
		name string
		b    QueryBuilder
		sql  string
		args []any
		err  error
	}{
		{
			name: "simple subquery",
			b: func() QueryBuilder {
				subquery := NewSelector[TestModel](db).Where(C("Id").Gt(10)).AsQuery()
				q := NewSelector[TestModel](db).From(subquery)
				return q
			}(),
			sql:  "SELECT * FROM (SELECT * FROM `test_model` WHERE `id`>?);",
			args: []any{10},
		},
		{
			name: "subquery with alias",
			b: func() QueryBuilder {
				subquery := NewSelector[TestModel](db).Where(C("Id").Gt(10)).AsQuery()
				q := NewSelector[TestModel](db).From(subquery.As("t1"))
				return q
			}(),
			sql:  "SELECT * FROM (SELECT * FROM `test_model` WHERE `id`>?) AS `t1`;",
			args: []any{10},
		},
		{
			name: "select subquery's column",
			b: func() QueryBuilder {
				t1 := NewSelector[TestModel](db).Where(C("Id").Gt(10)).AsQuery().As("t1")
				q := NewSelector[TestModel](db).From(t1).Select(t1.C("FirstName"), C("Age"))
				return q
			}(),
			sql:  "SELECT `t1`.`first_name`, `age` FROM (SELECT * FROM `test_model` WHERE `id`>?) AS `t1`;",
			args: []any{10},
		},
		{
			name: "select subquery's error column",
			b: func() QueryBuilder {
				t1 := NewSelector[TestModel](db).Where(C("Id").Gt(10)).AsQuery().As("t1")
				q := NewSelector[TestModel](db).From(t1).Select(t1.C("Unknown"))
				return q
			}(),
			err: errs.NewErrUnknownField("Unknown"),
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			q, err := tc.b.Build()
			if tc.err != nil {
				require.Equal(t, tc.err, err)
				return
			}
			require.Equal(t, tc.sql, q.SQL)
			require.Equal(t, tc.args, q.Args)
		})
	}
}
