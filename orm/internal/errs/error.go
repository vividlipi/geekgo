package errs

import (
	"errors"
	"fmt"
)

var (
	// ErrPointerOnly 只支持一级指针作为输入
	// 看到这个 error 说明你输入了其它的东西
	// 我们并不希望用户能够直接使用 err == ErrPointerOnly
	// 所以放在我们的 internal 包里
	ErrPointerOnly            = errors.New("orm: 只支持一级指针作为输入，例如 *User")
	ErrNoRows                 = errors.New("orm: 查询结果没有行返回")
	ErrTooManyReturnedColumns = errors.New("orm: 返回列数目超过类型field数目")
	ErrInsertNoRows           = errors.New("orm: 没有有效的插入行")
)

// NewErrUnknownField 返回代表未知字段的错误
// 一般意味着你可能输入的是列名，或者输入了错误的字段名
func NewErrUnknownField(fd string) error {
	return fmt.Errorf("orm: 未知字段 %s", fd)
}

func NewErrInvalidTagContent(content string) error {
	return fmt.Errorf("orm: Tag Format Wrong: %s", content)
}

func NewErrUnknownColumn(column string) error {
	return fmt.Errorf("orm: Unknow returned column from sql: %s", column)
}

func NewErrUnsupportedAssignableType(exp any) error {
	return fmt.Errorf("orm: 不支持的 Assignable 表达式 %v", exp)
}

func NewErrFailToRollbackTx(bizErr error, rbErr error, panicked bool) error {
	return fmt.Errorf("orm: 回滚事务失败, 业务错误 %w, 回滚错误 %s, panic: %t",
		bizErr, rbErr.Error(), panicked)
}

// NewErrUnsupportedExpressionType 返回一个不支持该 expression 错误信息
func NewErrUnsupportedExpressionType(exp any) error {
	return fmt.Errorf("orm: 不支持的表达式 %v", exp)
}
