package valuer

import (
	"database/sql"
	"geekgo/orm/internal/errs"
	"geekgo/orm/model"
	"reflect"
)

// reflectValue 基于反射的 Value
type reflectValue struct {
	val  reflect.Value
	meta *model.Model
}

var _ Creator = NewReflectValue

// NewReflectValue 返回一个封装好的，基于反射实现的 Value
// 输入 val 必须是一个指向结构体实例的指针，而不能是任何其它类型
func NewReflectValue(val any, meta *model.Model) Value {
	return reflectValue{
		val:  reflect.ValueOf(val).Elem(),
		meta: meta,
	}
}

func (r reflectValue) Field(name string) (any, error) {
	fd, ok := r.meta.FieldMap[name]
	if !ok {
		return nil, errs.NewErrUnknownField(name)
	}
	return r.val.Field(fd.Index).Interface(), nil
}

func (r reflectValue) SetColumns(rows *sql.Rows) error {
	columns, err := rows.Columns()

	if err != nil {
		return err
	}

	if len(columns) > len(r.meta.FieldMap) {
		return errs.ErrTooManyReturnedColumns
	}

	values_p := make([]any, 0, len(columns))
	values_e := make([]reflect.Value, 0, len(columns))

	for _, column := range columns {
		field, ok := r.meta.ColumnMap[column]
		if !ok {
			return errs.NewErrUnknownColumn(column)
		}
		v := reflect.New(field.Typ)
		values_p = append(values_p, v.Interface())
		values_e = append(values_e, v.Elem())
	}

	if err = rows.Scan(values_p...); err != nil {
		return err
	}

	for i, column := range columns {
		field := r.meta.ColumnMap[column]
		r.val.FieldByName(field.GoName).Set(values_e[i])
	}

	return nil
}
