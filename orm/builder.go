package orm

import (
	"geekgo/orm/internal/errs"
	"geekgo/orm/model"
	"strings"
)

type builder struct {
	r       model.Registry
	sb      strings.Builder
	args    []any
	model   *model.Model
	dialect Dialect
}

func (b *builder) InjectModel(model *model.Model) {
	b.model = model
}

func (b *builder) GetTableName() string {
	return b.model.TableName
}

func (b *builder) Build(predicate ...Predicate) error {
	condition := predicate[0]
	for _, predicate := range predicate[1:] {
		condition = condition.And(predicate)
	}
	return b.buildExpression(condition)
}

// buildColumn 构造列
// 如果 table 没有指定，我们就用 model 来判断列是否存在
func (b *builder) buildColumn(table TableReference, fd string) error {
	var alias string
	if table != nil {
		alias = table.tableAlias()
	}
	if alias != "" {
		b.writeWrap(alias)
		b.sb.WriteByte('.')
	}
	colName, err := b.colName(table, fd)
	if err != nil {
		return err
	}
	b.writeWrap(colName)
	return nil
}

func (b *builder) colName(table TableReference, fd string) (string, error) {
	switch tab := table.(type) {
	case nil:
		fdMeta, ok := b.model.FieldMap[fd]
		if !ok {
			return "", errs.NewErrUnknownField(fd)
		}
		return fdMeta.ColName, nil
	case Table:
		m, err := b.r.Get(tab.entity)
		if err != nil {
			return "", err
		}
		fdMeta, ok := m.FieldMap[fd]
		if !ok {
			return "", errs.NewErrUnknownField(fd)
		}
		return fdMeta.ColName, nil
	case SubQuery:
		fdMeta, ok := tab.model.FieldMap[fd]
		if !ok {
			return "", errs.NewErrUnknownField(fd)
		}
		return fdMeta.ColName, nil
	default:
		return "", errs.NewErrUnsupportedExpressionType(tab)
	}
}

func (b *builder) buildExpression(expression Expression) error {
	switch item := expression.(type) {
	case Column:
		return b.buildColumn(item.table, item.name)
	case value:
		b.sb.WriteByte('?')
		b.appendArgs(item.val)
	case Predicate:
		if item.op == opAnd || item.op == opOr {
			b.sb.WriteByte('(')
		}
		if err := b.buildExpression(item.left); err != nil {
			return err
		}
		if item.op == opAnd || item.op == opOr {
			b.sb.WriteByte(')')
		}
		b.sb.WriteString(item.op.String())
		if item.op == opAnd || item.op == opOr || item.op == opNot {
			b.sb.WriteByte('(')
		}
		if err := b.buildExpression(item.right); err != nil {
			return err
		}
		if item.op == opAnd || item.op == opOr || item.op == opNot {
			b.sb.WriteByte(')')
		}
	case RawExpression:
		b.sb.WriteString(item.raw)
		b.appendArgs(item.args...)
	case Aggregate:
		if item.alias != "" {
			b.writeWrap(item.alias)
		} else {
			fd, ok := b.model.FieldMap[item.arg]
			if !ok {
				return errs.NewErrUnknownField(item.arg)
			}
			b.sb.WriteString(item.fn)
			b.sb.WriteString("(")
			b.writeWrap(fd.ColName)
			b.sb.WriteString(")")
		}
	}
	return nil
}

func (b *builder) appendArgs(args ...any) {
	if len(args) == 0 {
		return
	}
	b.args = append(b.args, args...)
}

func (b *builder) writeWrap(m string) {
	b.sb.WriteString(b.dialect.quote() + m + b.dialect.quote())
}

func (b *builder) buildAs(alias string) {
	if alias != "" {
		b.sb.WriteString(" AS ")
		b.writeWrap(alias)
	}
}
