package orm

type Column struct {
	name  string
	alias string
	table TableReference
}

// expr 满足 Expression 的接口，可以作为构成条件的元素使用
func (c Column) expr()       {}
func (c Column) groupable()  {}
func (c Column) selectable() {}
func (c Column) assignable() {}

func C(name string) Column {
	return Column{name: name}
}

func (c Column) ASC() Orderable {
	return OrderSC{
		item: c,
		asc:  true,
	}
}

func (c Column) DESC() Orderable {
	return OrderSC{
		item: c,
		asc:  false,
	}
}

func (c Column) As(alias string) Column {
	return Column{name: c.name, alias: alias, table: c.table}
}

func (c Column) Eq(val any) Predicate {
	return Predicate{
		left:  c,
		op:    opEq,
		right: exprOf(val),
	}
}

func (c Column) Gt(val any) Predicate {
	return Predicate{
		left:  c,
		op:    opGt,
		right: exprOf(val),
	}
}

func (c Column) Lt(val any) Predicate {
	return Predicate{
		left:  c,
		op:    opLt,
		right: exprOf(val),
	}
}
