package orm

type op string

const opEq op = "="
const opGt op = ">"
const opLt op = "<"
const opOr op = " OR "
const opAnd op = " AND "
const opNot op = "NOT "

func (op op) String() string {
	return string(op)
}

// value 将任何类型包装成 Expression 类型
type value struct{ val any }

func (v value) expr() {}

func valueOf(val any) Expression {
	switch item := val.(type) {
	case RawExpression:
		return item
	default:
		return value{val: val}
	}
}

// Predicate where 条件的具体表示
type Predicate struct {
	left  Expression
	op    op
	right Expression
}

func (p Predicate) expr() {}

func Not(d Predicate) Predicate {
	return Predicate{
		op:    opNot,
		right: d,
	}
}

func (p Predicate) And(d Predicate) Predicate {
	return Predicate{
		left:  p,
		op:    opAnd,
		right: d,
	}
}

func (p Predicate) Or(d Predicate) Predicate {
	return Predicate{
		left:  p,
		op:    opOr,
		right: d,
	}
}
