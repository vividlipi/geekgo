package orm

type Assignable interface {
	assignable()
}

type Assignment struct {
	col string
	val any
}

func Assign(col string, val any) Assignment {
	return Assignment{col: col, val: val}
}

func (Assignment) assignable() {}
