package orm

import (
	"context"
	"database/sql"
	"errors"
	"geekgo/orm/internal/errs"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestSelector_Join(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)

	type Order struct {
		Id        int
		UsingCol1 string
		UsingCol2 string
	}

	type OrderDetail struct {
		OrderId int
		ItemId  int

		UsingCol1 string
		UsingCol2 string
	}

	type Item struct {
		Id int
	}

	testCases := []struct {
		name      string
		q         QueryBuilder
		wantQuery *Query
		wantErr   error
	}{
		{
			// 虽然泛型是 Order，但是我们传入 OrderDetail
			name: "specify table",
			q:    NewSelector[Order](db).From(TableOf(&OrderDetail{})),
			wantQuery: &Query{
				SQL: "SELECT * FROM `order_detail`;",
			},
		},
		{
			name: "join",
			q: func() QueryBuilder {
				t1 := TableOf(&Order{}).As("t1")
				t2 := TableOf(&OrderDetail{})
				return NewSelector[Order](db).From(t1.Join(t2).On(t1.C("Id").Eq(t2.C("OrderId"))))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM (`order` AS `t1` JOIN `order_detail` ON `t1`.`id`=`order_id`);",
			},
		},
		{
			name: "multiple join",
			q: func() QueryBuilder {
				t1 := TableOf(&Order{}).As("t1")
				t2 := TableOf(&OrderDetail{}).As("t2")
				t3 := TableOf(&Item{}).As("t3")
				return NewSelector[Order](db).
					From(t1.Join(t2).
						On(t1.C("Id").Eq(t2.C("OrderId"))).
						Join(t3).On(t2.C("ItemId").Eq(t3.C("Id"))))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM ((`order` AS `t1` JOIN `order_detail` AS `t2` ON `t1`.`id`=`t2`.`order_id`) JOIN `item` AS `t3` ON `t2`.`item_id`=`t3`.`id`);",
			},
		},
		{
			name: "left multiple join",
			q: func() QueryBuilder {
				t1 := TableOf(&Order{}).As("t1")
				t2 := TableOf(&OrderDetail{}).As("t2")
				t3 := TableOf(&Item{}).As("t3")
				return NewSelector[Order](db).
					From(t1.LeftJoin(t2).
						On(t1.C("Id").Eq(t2.C("OrderId"))).
						LeftJoin(t3).On(t2.C("ItemId").Eq(t3.C("Id"))))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM ((`order` AS `t1` LEFT JOIN `order_detail` AS `t2` ON `t1`.`id`=`t2`.`order_id`) LEFT JOIN `item` AS `t3` ON `t2`.`item_id`=`t3`.`id`);",
			},
		},
		{
			name: "right multiple join",
			q: func() QueryBuilder {
				t1 := TableOf(&Order{}).As("t1")
				t2 := TableOf(&OrderDetail{}).As("t2")
				t3 := TableOf(&Item{}).As("t3")
				return NewSelector[Order](db).
					From(t1.RightJoin(t2).
						On(t1.C("Id").Eq(t2.C("OrderId"))).
						RightJoin(t3).On(t2.C("ItemId").Eq(t3.C("Id"))))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM ((`order` AS `t1` RIGHT JOIN `order_detail` AS `t2` ON `t1`.`id`=`t2`.`order_id`) RIGHT JOIN `item` AS `t3` ON `t2`.`item_id`=`t3`.`id`);",
			},
		},

		{
			name: "join multiple using",
			q: func() QueryBuilder {
				t1 := TableOf(&Order{}).As("t1")
				t2 := TableOf(&OrderDetail{})
				return NewSelector[Order](db).
					From(t1.Join(t2).Using("UsingCol1", "UsingCol2"))
			}(),
			wantQuery: &Query{
				SQL: "SELECT * FROM (`order` AS `t1` JOIN `order_detail` USING (`using_col1`,`using_col2`));",
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			query, err := tc.q.Build()
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantQuery, query)
		})
	}
}

func TestSelector_As(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "column as",
			builder:   NewSelector[TestModel](db).Select(C("FirstName").As("name")),
			wantQuery: &Query{SQL: "SELECT `first_name` AS `name` FROM `test_model`;", Args: nil},
		},
		{
			name:      "aggregate as",
			builder:   NewSelector[TestModel](db).Select(Max("Id").As("total")),
			wantQuery: &Query{SQL: "SELECT MAX(`id`) AS `total` FROM `test_model`;", Args: nil},
		},
	}

	RunTestCases(t, testCases)
}

func TestSelector_SelectRawExpression(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "raw expression",
			builder:   NewSelector[TestModel](db).Select(Raw("COUNT(DISTINCT `age`)")),
			wantQuery: &Query{SQL: "SELECT COUNT(DISTINCT `age`) FROM `test_model`;", Args: nil},
		},
		{
			name:      "raw expression having args",
			builder:   NewSelector[TestModel](db).Select(Raw("HAVING `age` > ?", 18)),
			wantQuery: &Query{SQL: "SELECT HAVING `age` > ? FROM `test_model`;", Args: []any{18}},
		},
	}

	RunTestCases(t, testCases)
}
func TestSelector_SelectAggregate(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "sum",
			builder:   NewSelector[TestModel](db).Select(Sum("Age")),
			wantQuery: &Query{SQL: "SELECT SUM(`age`) FROM `test_model`;", Args: nil},
			wantErr:   nil,
		},
		{
			name:      "max",
			builder:   NewSelector[TestModel](db).Select(Max("Age")),
			wantQuery: &Query{SQL: "SELECT MAX(`age`) FROM `test_model`;", Args: nil},
			wantErr:   nil,
		},
		{
			name:      "min",
			builder:   NewSelector[TestModel](db).Select(Min("Age")),
			wantQuery: &Query{SQL: "SELECT MIN(`age`) FROM `test_model`;", Args: nil},
			wantErr:   nil,
		},
		{
			name:      "multiple",
			builder:   NewSelector[TestModel](db).Select(Max("Age"), Min("Age"), C("Age")),
			wantQuery: &Query{SQL: "SELECT MAX(`age`), MIN(`age`), `age` FROM `test_model`;", Args: nil},
			wantErr:   nil,
		},
		{
			name:    "multiple",
			builder: NewSelector[TestModel](db).Select(Max("age"), Min("Age"), C("Age")),
			wantErr: errs.NewErrUnknownField("age"),
		},
	}

	RunTestCases(t, testCases)
}

func TestSelector_SelectColumn(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "select columns",
			builder:   NewSelector[TestModel](db).Select(C("Id"), C("FirstName")),
			wantQuery: &Query{SQL: "SELECT `id`, `first_name` FROM `test_model`;", Args: nil},
			wantErr:   nil,
		},
	}

	RunTestCases(t, testCases)
}

// func TestSelector_Build_From_Table(t *testing.T) {
// 	mockDB, _, _ := sqlmock.New()
// 	db, _ := OpenDB(mockDB)
// 	testCases := []TestCase{
// 		{
// 			name:      "without from table name, will auto use Object Type Name",
// 			builder:   NewSelector[TestModel](db),
// 			wantQuery: &Query{SQL: "SELECT * FROM `test_model`;", Args: nil},
// 			wantErr:   nil,
// 		},
// 		{
// 			name:      "with from table name, will use parameter passed in",
// 			builder:   (NewSelector[TestModel](db)).From("testmodel"),
// 			wantQuery: &Query{SQL: "SELECT * FROM `testmodel`;", Args: nil},
// 			wantErr:   nil,
// 		},
// 		{
// 			name:      "with from db.table, will use parameter passed in",
// 			builder:   (NewSelector[TestModel](db)).From("db.TestModel"),
// 			wantQuery: &Query{SQL: "SELECT * FROM `db.TestModel`;", Args: nil},
// 			wantErr:   nil,
// 		},
// 	}

// 	RunTestCases(t, testCases)
// }

func TestSelector_Build_Where(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "where id = 20231024",
			builder:   (NewSelector[TestModel](db)).Where(C("Id").Eq("20231024")),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE `id`=?;", Args: []any{"20231024"}},
			wantErr:   nil,
		},
		{
			name:      "where id > 20231024",
			builder:   (NewSelector[TestModel](db)).Where(C("Id").Gt("20231024")),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE `id`>?;", Args: []any{"20231024"}},
			wantErr:   nil,
		},
		{
			name:      "where id < 20231024",
			builder:   (NewSelector[TestModel](db)).Where(C("Id").Lt("20231024")),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE `id`<?;", Args: []any{"20231024"}},
			wantErr:   nil,
		},
		{
			name:      "where id < 20231024 and age > 18",
			builder:   (NewSelector[TestModel](db)).Where(C("Id").Lt("20231024").And(C("Age").Gt(18))),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE (`id`<?) AND (`age`>?);", Args: []any{"20231024", 18}},
			wantErr:   nil,
		},
		{
			name:      "where id < 20231024, age > 18",
			builder:   (NewSelector[TestModel](db)).Where(C("Id").Lt("20231024"), C("Age").Gt(18)),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE (`id`<?) AND (`age`>?);", Args: []any{"20231024", 18}},
			wantErr:   nil,
		},
		{
			name:      "where id < 20231024 and age > 18",
			builder:   (NewSelector[TestModel](db)).Where(C("Id").Lt("20231024").Or(C("Age").Gt(18))),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE (`id`<?) OR (`age`>?);", Args: []any{"20231024", 18}},
			wantErr:   nil,
		},
		{
			name:      "where not id < 20231024",
			builder:   (NewSelector[TestModel](db)).Where(Not(C("Id").Lt("20231024"))),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE NOT (`id`<?);", Args: []any{"20231024"}},
			wantErr:   nil,
		},
		{
			name:      "where (not (id < 20231024)) and (age = 18)",
			builder:   (NewSelector[TestModel](db)).Where(Not(C("Id").Lt("20231024")), C("Age").Eq(18)),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE (NOT (`id`<?)) AND (`age`=?);", Args: []any{"20231024", 18}},
			wantErr:   nil,
		},
		{
			name:      "where not ((id < 20231024) and (age = 18))",
			builder:   (NewSelector[TestModel](db)).Where(Not(C("Id").Lt("20231024").And(C("Age").Eq(18)))),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE NOT ((`id`<?) AND (`age`=?));", Args: []any{"20231024", 18}},
			wantErr:   nil,
		},
		{
			name:      "where raw expression",
			builder:   (NewSelector[TestModel](db)).Where(Raw("`id` = `age` + 1").AsPredicate()),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE `id` = `age` + 1;", Args: nil},
			wantErr:   nil,
		},
		{
			name:      "where raw expression with args",
			builder:   (NewSelector[TestModel](db)).Where(Raw("`id` = `age` + ?", 1).AsPredicate()),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE `id` = `age` + ?;", Args: []any{1}},
			wantErr:   nil,
		},
		{
			name:      "where id equal raw expression with args",
			builder:   (NewSelector[TestModel](db)).Where(C("Id").Eq(Raw("`age` + ?", 1))),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` WHERE `id`=`age` + ?;", Args: []any{1}},
			wantErr:   nil,
		},
	}

	RunTestCases(t, testCases)
}

type TestModel struct {
	Id        int64
	FirstName string
	Age       int8
	LastName  *sql.NullString
}

func TestGet(t *testing.T) {
	mockDB, mockSQL, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	mockRows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	mockRows.AddRow("1", "Tom", "18", "Jerry")
	mockSQL.ExpectQuery("SELECT .*").WillReturnRows(mockRows)
	selector := NewSelector[TestModel](db).Where(C("Id").Eq(1))
	got, _ := selector.Get(context.Background())
	want := &TestModel{
		Id:        1,
		FirstName: "Tom",
		Age:       18,
		LastName: &sql.NullString{
			String: "Jerry",
			Valid:  true,
		},
	}
	assert.Equal(t, want, got)
}

func TestGet_BuildErrWillReturnNil(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	selector := NewSelector[TestModel](db).Where(C("ID").Eq(1))
	got, err := selector.Get(context.Background())
	assert.Equal(t, err, errs.NewErrUnknownField("ID"))
	assert.Nil(t, got)
}

func TestGet_DBQueryErrWillReturnNil(t *testing.T) {
	mockDB, mockSQL, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	stubErr := errors.New("SQL run Error")
	mockSQL.ExpectQuery("SELECT .*").WillReturnError(stubErr)
	selector := NewSelector[TestModel](db).Where(C("Id").Eq(1))
	got, err := selector.Get(context.Background())
	assert.Equal(t, err, stubErr)
	assert.Nil(t, got)
}

func TestGet_NoRowsWillReturnNil(t *testing.T) {
	mockDB, mockSQL, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	mockRows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	mockSQL.ExpectQuery("SELECT .*").WillReturnRows(mockRows)
	selector := NewSelector[TestModel](db).Where(C("Id").Eq(1))
	got, err := selector.Get(context.Background())
	assert.Equal(t, err, errs.ErrNoRows)
	assert.Nil(t, got)
}

func TestGet_ScanTypeNotPairWillReturnNil(t *testing.T) {
	mockDB, mockSQL, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	mockRows := sqlmock.NewRows([]string{"id", "first_name", "age", "last_name"})
	mockRows.AddRow("abc", "Tom", "18", "Jerry")
	mockSQL.ExpectQuery("SELECT .*").WillReturnRows(mockRows)
	selector := NewSelector[TestModel](db).Where(C("Id").Eq(1))
	got, err := selector.Get(context.Background())
	assert.NotNil(t, err)
	assert.Nil(t, got)
}

func TestSelect_Group(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "group by 1 colum",
			builder:   NewSelector[TestModel](db).GroupBy(C("FirstName")),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` GROUP BY `first_name`;"},
			wantErr:   nil,
		},
		{
			name: "group by 1 colum and has alias",
			builder: func() *Selector[TestModel] {
				id := C("Id")
				first_name := C("FirstName").As("name")
				return NewSelector[TestModel](db).Select(id, first_name).GroupBy(first_name)
			}(),
			wantQuery: &Query{SQL: "SELECT `id`, `first_name` AS `name` FROM `test_model` GROUP BY `name`;"},
			wantErr:   nil,
		},
		{
			name: "group by Multi colums and some has alias",
			builder: func() *Selector[TestModel] {
				id := C("Id")
				first_name := C("FirstName").As("name")
				return NewSelector[TestModel](db).Select(id, first_name).GroupBy(id, first_name)
			}(),
			wantQuery: &Query{SQL: "SELECT `id`, `first_name` AS `name` FROM `test_model` GROUP BY `id`, `name`;"},
			wantErr:   nil,
		},
		{
			name: "group by Multi colums and some has alias",
			builder: func() *Selector[TestModel] {
				g := Raw("`id` % `age`")
				return NewSelector[TestModel](db).GroupBy(g)
			}(),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` GROUP BY `id` % `age`;"},
			wantErr:   nil,
		},
	}

	RunTestCases(t, testCases)
}

func TestSelect_Having(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "having id > 300 and age > 18",
			builder:   NewSelector[TestModel](db).Having(C("Id").Gt(300), C("Age").Gt(18)),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` HAVING (`id`>?) AND (`age`>?);", Args: []any{300, 18}},
			wantErr:   nil,
		},
		{
			name:      "group by firstname having average age > 18",
			builder:   NewSelector[TestModel](db).GroupBy(C("FirstName")).Having(Sum("Age").Gt(18)),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` GROUP BY `first_name` HAVING SUM(`age`)>?;", Args: []any{18}},
			wantErr:   nil,
		},
		{
			name: "group by firstname having average age > 18",
			builder: func() *Selector[TestModel] {
				avg := Sum("Age").As("valid")
				return NewSelector[TestModel](db).Select(avg).GroupBy(C("FirstName")).Having(avg.Gt(18))
			}(),
			wantQuery: &Query{SQL: "SELECT SUM(`age`) AS `valid` FROM `test_model` GROUP BY `first_name` HAVING `valid`>?;", Args: []any{18}},
			wantErr:   nil,
		},
	}

	RunTestCases(t, testCases)
}

func TestSelect_Order(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:      "order id asc",
			builder:   NewSelector[TestModel](db).OrderBy(C("Id").ASC()),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` ORDER BY `id` ASC;"},
			wantErr:   nil,
		},
		{
			name:      "multiple order by",
			builder:   NewSelector[TestModel](db).OrderBy(C("Id").DESC(), C("FirstName").ASC()),
			wantQuery: &Query{SQL: "SELECT * FROM `test_model` ORDER BY `id`, `first_name` ASC;"},
			wantErr:   nil,
		},
		{
			name: "order with alias",
			builder: func() *Selector[TestModel] {
				avg := C("Age").As("valid")
				return NewSelector[TestModel](db).Select(avg).OrderBy(avg.DESC())
			}(),
			wantQuery: &Query{SQL: "SELECT `age` AS `valid` FROM `test_model` ORDER BY `valid`;"},
			wantErr:   nil,
		},
	}

	RunTestCases(t, testCases)
}

func TestSelect_LimitSqlite(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB, WithDialect(DialectSqlite3))
	testcases := []TestCase{
		{
			name: "limit sqlite",
			builder: func() *Selector[TestModel] {
				avg := C("Age").As("valid")
				return NewSelector[TestModel](db).Select(avg).OrderBy(avg.DESC()).Limit(4, 7)
			}(),
			wantQuery: &Query{SQL: "SELECT `age` AS `valid` FROM `test_model` ORDER BY `valid` OFFSET 4 LIMIT 7;"},
			wantErr:   nil,
		},
	}
	RunTestCases(t, testcases)
}

func TestSelect_LimitMysql(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB, WithDialect(DialectMysql))
	testcases := []TestCase{
		{
			name: "limit sqlite",
			builder: func() *Selector[TestModel] {
				avg := C("Age").As("valid")
				return NewSelector[TestModel](db).Select(avg).OrderBy(avg.DESC()).Limit(4, 7)
			}(),
			wantQuery: &Query{SQL: "SELECT `age` AS `valid` FROM `test_model` ORDER BY `valid` LIMIT 4,7;"},
			wantErr:   nil,
		},
	}
	RunTestCases(t, testcases)
}
