package orm

import (
	"context"
	"database/sql"
	"geekgo/orm/internal/errs"
	"geekgo/orm/model"
)

type OnDuplicateKey struct {
	assigns      []Assignable
	conflictCols []Column
}

type OnDuplicateKeyBuilder[T any] struct {
	i            *Insertor[T]
	conflictCols []Column
}

func (o *OnDuplicateKeyBuilder[T]) OnConflict(cols ...Column) *OnDuplicateKeyBuilder[T] {
	o.conflictCols = cols
	return o
}

func (o *OnDuplicateKeyBuilder[T]) Update(assigns ...Assignable) *Insertor[T] {
	o.i.onDuplicate = &OnDuplicateKey{
		assigns:      assigns,
		conflictCols: o.conflictCols,
	}
	return o.i
}

type Insertor[T any] struct {
	builder
	cols        []Column
	values      []*T
	onDuplicate *OnDuplicateKey
	table       string
	core
	session Session
}

func NewInsertor[T any](session Session) *Insertor[T] {
	core := session.getCore()
	model, err := core.r.Get(new(T))
	if err != nil {
		panic("Failed to map Type to Model")
	}
	return &Insertor[T]{
		builder: builder{
			dialect: core.dialect,
			model:   model,
		},
		core:    session.getCore(),
		session: session,
	}
}

func (i *Insertor[T]) Columns(cols ...Column) *Insertor[T] {
	i.cols = cols
	return i
}

func (i *Insertor[T]) OnDuplicateKey() *OnDuplicateKeyBuilder[T] {
	return &OnDuplicateKeyBuilder[T]{
		i: i,
	}
}

func (i *Insertor[T]) Values(values ...*T) *Insertor[T] {
	i.values = values
	return i
}

func (i *Insertor[T]) From(table string) *Insertor[T] {
	i.table = table
	return i
}

func (i *Insertor[T]) Build() (*Query, error) {
	if len(i.values) == 0 {
		return nil, errs.ErrInsertNoRows
	}

	i.sb.WriteString("INSERT INTO ")

	if i.table == "" {
		i.writeWrap(i.GetTableName())
	} else {
		i.writeWrap(i.table)
	}

	fields := i.model.Fields
	if len(i.cols) != 0 {
		fields = make([]*model.Field, 0, len(i.cols))
		for _, c := range i.cols {
			field, ok := i.model.FieldMap[c.name]
			if !ok {
				return nil, errs.NewErrUnknownField(c.name)
			}
			fields = append(fields, field)
		}
	}
	i.sb.WriteString(" (")
	for idx, fd := range fields {
		if idx > 0 {
			i.sb.WriteByte(',')
		}
		i.writeWrap(fd.ColName)
	}
	i.sb.WriteString(") VALUES ")
	for vIdx, val := range i.values {
		if vIdx > 0 {
			i.sb.WriteByte(',')
		}
		refVal := i.core.valCreator(val, i.model)
		i.sb.WriteByte('(')
		for fIdx, field := range fields {
			if fIdx > 0 {
				i.sb.WriteByte(',')
			}
			i.sb.WriteByte('?')
			fdVal, _ := refVal.Field(field.GoName)
			i.args = append(i.args, fdVal)
		}
		i.sb.WriteByte(')')
	}
	if i.onDuplicate != nil {
		i.core.dialect.buildOnDuplicateKey(&i.builder, i.onDuplicate)
	}
	i.sb.WriteByte(';')
	return &Query{SQL: i.sb.String(), Args: i.args}, nil
}

func (i *Insertor[T]) Exec(ctx context.Context) sql.Result {
	return exec(ctx, i.session, i.core, &QueryContext{
		Builder: i,
		Type:    "INSERT",
	})
}
