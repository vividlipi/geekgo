package orm

import (
	"geekgo/orm/internal/errs"
	"strconv"
)

var DialectMysql Dialect = mysqlDialect{}
var DialectSqlite3 Dialect = sqlite3Dialect{}

type Dialect interface {
	buildOnDuplicateKey(*builder, *OnDuplicateKey) error
	quote() string
	buildLimit(offset, limit int) string
}

type mysqlDialect struct{}

func (m mysqlDialect) buildOnDuplicateKey(b *builder, key *OnDuplicateKey) error {
	b.sb.WriteString(" ON DUPLICATE KEY UPDATE ")
	for idx, assign := range key.assigns {
		if idx > 0 {
			b.sb.WriteByte(',')
		}
		switch assignment := assign.(type) {
		case Column:
			fd, ok := b.model.FieldMap[assignment.name]
			if !ok {
				return errs.NewErrUnknownField(assignment.name)
			}
			b.writeWrap(fd.ColName)
			b.sb.WriteString("=VALUES(")
			b.writeWrap(fd.ColName)
			b.sb.WriteString(")")
		case Assignment:
			fd, ok := b.model.FieldMap[assignment.col]
			if !ok {
				return errs.NewErrUnknownField(assignment.col)
			}
			b.writeWrap(fd.ColName)
			b.sb.WriteString("=?")
			b.args = append(b.args, assignment.val)
		default:
			return errs.NewErrUnsupportedAssignableType(assignment)
		}
	}
	return nil
}

func (m mysqlDialect) quote() string {
	return "`"
}
func (m mysqlDialect) buildLimit(offset, limit int) string {
	return " LIMIT " + strconv.Itoa(offset) + "," + strconv.Itoa(limit)
}

type sqlite3Dialect struct{}

func (m sqlite3Dialect) buildOnDuplicateKey(b *builder, key *OnDuplicateKey) error {
	b.sb.WriteString(" ON CONFLICT")
	if len(key.conflictCols) > 0 {
		b.sb.WriteByte('(')
		for i, col := range key.conflictCols {
			if i > 0 {
				b.sb.WriteByte(',')
			}
			fd, ok := b.model.FieldMap[col.name]
			if !ok {
				return errs.NewErrUnknownField(col.name)
			}
			b.writeWrap(fd.ColName)
		}
		b.sb.WriteByte(')')
	}
	b.sb.WriteString(" DO UPDATE SET ")

	for idx, a := range key.assigns {
		if idx > 0 {
			b.sb.WriteByte(',')
		}
		switch assign := a.(type) {
		case Column:
			fd, ok := b.model.FieldMap[assign.name]
			if !ok {
				return errs.NewErrUnknownField(assign.name)
			}
			b.writeWrap(fd.ColName)
			b.sb.WriteString("=excluded.")
			b.writeWrap(fd.ColName)
		case Assignment:
			fd, ok := b.model.FieldMap[assign.col]
			if !ok {
				return errs.NewErrUnknownField(assign.col)
			}
			b.writeWrap(fd.ColName)
			b.sb.WriteString("=?")
			b.args = append(b.args, assign.val)
		default:
			return errs.NewErrUnsupportedAssignableType(a)
		}
	}
	return nil
}

func (m sqlite3Dialect) quote() string {
	return "`"
}

func (m sqlite3Dialect) buildLimit(offset, limit int) string {
	return " OFFSET " + strconv.Itoa(offset) + " LIMIT " + strconv.Itoa(limit)
}
