package orm

import (
	"context"
	"database/sql"
	"geekgo/orm/internal/errs"
	"geekgo/orm/internal/valuer"
	"geekgo/orm/model"
)

type DBOption func(*DB)

type DB struct {
	core
	db *sql.DB
}

func Open(driverName, dataSourceName string, opts ...DBOption) (*DB, error) {
	// 公开方法返回 error
	// 若后面再加 error，会导致不兼容
	sqldb, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}

	db := &DB{
		core: core{
			r:          model.NewRegistry(),
			valCreator: valuer.NewUnsafeValue,
			dialect:    DialectMysql,
		},
		db: sqldb,
	}

	for _, opt := range opts {
		opt(db)
	}

	return db, nil
}

func OpenDB(sqldb *sql.DB, opts ...DBOption) (*DB, error) {
	db := &DB{
		core: core{
			r:          model.NewRegistry(),
			valCreator: valuer.NewUnsafeValue,
			dialect:    DialectMysql,
		},
		db: sqldb,
	}

	for _, opt := range opts {
		opt(db)
	}

	return db, nil
}
func (db *DB) getCore() core {
	return db.core
}

func (db *DB) NewTx(ctx context.Context, opts *sql.TxOptions) (*Tx, error) {
	tx, err := db.db.BeginTx(ctx, opts)
	if err != nil {
		return nil, err
	}
	return &Tx{tx: tx}, nil
}

func (db *DB) DoTx(ctx context.Context, fn func(ctx context.Context, tx *Tx) error, opts *sql.TxOptions) (err error) {
	var tx *Tx
	tx, err = db.NewTx(ctx, opts)
	if err != nil {
		return err
	}

	panicked := true
	defer func() {
		if panicked || err != nil {
			e := tx.Rollback()
			if e != nil {
				err = errs.NewErrFailToRollbackTx(err, e, panicked)
			}
		} else {
			err = tx.Commit()
		}
	}()

	err = fn(ctx, tx)
	panicked = false
	return err
}

func (db *DB) ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error) {
	return db.db.ExecContext(ctx, query, args...)
}
func (db *DB) QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error) {
	return db.db.QueryContext(ctx, query, args...)
}

func DBWithMiddleware(ms ...Middleware) DBOption {
	return func(db *DB) {
		db.ms = ms
	}
}

func WithReflectValuer() DBOption {
	return func(db *DB) {
		db.valCreator = valuer.NewReflectValue
	}
}

func WithUnsafeValuer() DBOption {
	return func(db *DB) {
		db.valCreator = valuer.NewUnsafeValue
	}
}

func WithDialect(dialect Dialect) DBOption {
	return func(db *DB) {
		db.dialect = dialect
	}
}
