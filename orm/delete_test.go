package orm

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestDeleter_Build(t *testing.T) {
	mockDB, _, _ := sqlmock.New()
	db, _ := OpenDB(mockDB)
	testCases := []TestCase{
		{
			name:    "no where",
			builder: (NewDeleter[TestModel](db)).From("test_model"),
			wantQuery: &Query{
				SQL: "DELETE FROM `test_model`;",
			},
		},
		{
			name:    "where",
			builder: (NewDeleter[TestModel](db)).Where(C("Id").Eq(16)),
			wantQuery: &Query{
				SQL:  "DELETE FROM `test_model` WHERE `id`=?;",
				Args: []any{16},
			},
		},
		{
			name:    "from",
			builder: (NewDeleter[TestModel](db)).From("test_model").Where(C("Id").Eq(16)),
			wantQuery: &Query{
				SQL:  "DELETE FROM `test_model` WHERE `id`=?;",
				Args: []any{16},
			},
		},
	}

	RunTestCases(t, testCases)
}
