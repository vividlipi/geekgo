package reflect

import (
	"geekgo/orm/reflect/types"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIterateFunc(t *testing.T) {
	testCases := []struct {
		name    string
		entity  any
		wantRes map[string]FuncInfo
		wantErr error
	}{
		{
			name:   "struct call get",
			entity: types.NewOrder(10, "Tom"),
			wantRes: map[string]FuncInfo{
				"GetId": {
					Name:        "GetId",
					InputTypes:  []reflect.Type{reflect.TypeOf(types.NewOrder(0, ""))},
					OutputTypes: []reflect.Type{reflect.TypeOf(0)},
					Result:      []any{10},
				},
			},
		},
		{
			name:   "pointer",
			entity: types.NewOrderPtr(10, "Tom"),
			wantRes: map[string]FuncInfo{
				"GetId": {
					Name:        "GetId",
					InputTypes:  []reflect.Type{reflect.TypeOf(types.NewOrderPtr(0, ""))},
					OutputTypes: []reflect.Type{reflect.TypeOf(0)},
					Result:      []any{10},
				},
				"SetUser": {
					Name:        "SetUser",
					InputTypes:  []reflect.Type{reflect.TypeOf(types.NewOrderPtr(0, "")), reflect.TypeOf("")},
					OutputTypes: []reflect.Type{},
					Result:      []any{},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := IterateFunc(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err == nil {
				assert.Equal(t, tc.wantRes, res)
			}
		})
	}
}
