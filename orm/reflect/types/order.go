package types

type Order struct {
	Id   int
	user string
}

func NewOrder(id int, user string) Order {
	return Order{Id: id, user: user}
}

func NewOrderPtr(id int, user string) *Order {
	return &Order{Id: id, user: user}
}

func (o Order) GetId() int {
	return o.Id
}

func (o *Order) SetUser(user string) {
	o.user = user
}

func (o *Order) deleteUser(user string) {
	o.user = ""
}
