package reflect

import (
	"errors"
	"reflect"
)

func IterateField(entity any) (map[string]any, error) {
	typ := reflect.TypeOf(entity)
	if typ == nil {
		return nil, errors.New("unsupport nil")
	}

	val := reflect.ValueOf(entity)
	if val.IsZero() {
		return nil, errors.New("unsupport nil")
	}

	for typ.Kind() == reflect.Pointer {
		typ = typ.Elem()
		val = val.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return nil, errors.New("unsupport")
	}

	numField := typ.NumField()
	res := make(map[string]any, numField)
	for i := 0; i < numField; i++ {
		fieldType := typ.Field(i)
		fieldValue := val.Field(i)
		if fieldType.IsExported() {
			res[fieldType.Name] = fieldValue.Interface()
		} else {
			res[fieldType.Name] = reflect.Zero(fieldType.Type).Interface()
		}
	}
	return res, nil
}

func SetField(entity any, field string, newVal any) error {
	val := reflect.ValueOf(entity)
	for val.Type().Kind() == reflect.Pointer {
		val = val.Elem()
	}
	fieldVal := val.FieldByName(field)
	if !fieldVal.CanSet() {
		return errors.New("can not change")
	}
	fieldVal.Set(reflect.ValueOf(newVal))
	return nil
}
