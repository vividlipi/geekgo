package reflect

import (
	"errors"
	"geekgo/orm/reflect/types"
	"testing"

	"github.com/stretchr/testify/assert"
)

type User struct {
	Name string
	age  int
}

func TestIterateField(t *testing.T) {
	testCases := []struct {
		name    string
		entity  any
		wantErr error
		wantRes map[string]any
	}{
		{
			name:    "struct in package",
			entity:  User{Name: "Tom", age: 18},
			wantRes: map[string]any{"Name": "Tom", "age": 0},
		},
		{
			name:    "struct out package",
			entity:  types.NewOrder(20221011, "Tom"),
			wantRes: map[string]any{"Id": 20221011, "user": ""},
		},
		{
			name:    "struct pointer",
			entity:  &User{Name: "Tom", age: 18},
			wantRes: map[string]any{"Name": "Tom", "age": 0},
		},
		{
			name: "struct multi-pointer",
			entity: func() **User {
				user := &User{Name: "Tom", age: 18}
				return &user
			}(),
			wantRes: map[string]any{"Name": "Tom", "age": 0},
		},
		{
			name:    "basic type",
			entity:  18,
			wantErr: errors.New("unsupport"),
		},
		{
			name:    "nil",
			entity:  nil,
			wantErr: errors.New("unsupport nil"),
		},
		{
			name:    "type nil",
			entity:  (*User)(nil),
			wantErr: errors.New("unsupport nil"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := IterateField(tc.entity)
			assert.Equal(t, tc.wantErr, err)
			if err == nil {
				assert.Equal(t, tc.wantRes, res)
			}
		})
	}
}

func TestSetField(t *testing.T) {
	testCases := []struct {
		name       string
		entity     any
		field      string
		newVal     any
		wantErr    error
		wantEntity any
	}{
		{
			name:    "struct",
			entity:  User{Name: "Tom"},
			field:   "Name",
			newVal:  "Jerry",
			wantErr: errors.New("can not change"),
		},
		{
			name:       "struct pointer",
			entity:     &User{Name: "Tom"},
			field:      "Name",
			newVal:     "Jerry",
			wantEntity: &User{Name: "Jerry"},
		},
		{
			name:    "unexport",
			entity:  types.NewOrderPtr(2022, "Tom"),
			field:   "user",
			newVal:  "Jerry",
			wantErr: errors.New("can not change"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := SetField(tc.entity, tc.field, tc.newVal)
			assert.Equal(t, tc.wantErr, err)
			if err == nil {
				assert.Equal(t, tc.wantEntity, tc.entity)
			}
		})
	}
}
