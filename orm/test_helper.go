package orm

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	name      string
	builder   QueryBuilder
	wantQuery *Query
	wantErr   error
}

func RunTestCases(t *testing.T, testCases []TestCase) {
	t.Helper()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotQuery, gotErr := tc.builder.Build()
			assert.Equal(t, tc.wantErr, gotErr)
			if gotErr == nil {
				assert.Equal(t, tc.wantQuery, gotQuery)
			}
		})
	}
}

func GetLenOfSyncMap(m *sync.Map) (count int) {
	// var count int
	m.Range(func(key, value interface{}) bool {
		count++
		return true
	})
	return
}
