package orm

import (
	"context"
	"geekgo/orm/internal/errs"
)

type Selectable interface {
	selectable()
}

type Groupable interface {
	groupable()
}

type Selector[T any] struct {
	builder
	expr   []Selectable
	table  TableReference
	where  []Predicate
	having []Predicate
	group  []Groupable
	order  []Orderable
	offset int
	limit  int
	core
	session Session
}

func NewSelector[T any](session Session) *Selector[T] {
	core := session.getCore()
	model, err := core.r.Get(new(T))
	if err != nil {
		panic("Failed to map Type to Model")
	}

	return &Selector[T]{
		builder: builder{
			r:       core.r,
			dialect: core.dialect,
			model:   model,
		},
		core:    core,
		session: session,
	}
}

func (s *Selector[T]) AsQuery() SubQuery {
	return SubQuery{s: s, model: s.model}
}

func (s *Selector[T]) Build() (*Query, error) {
	// From
	s.sb.WriteString("SELECT ")
	if err := s.buildSelectItem(); err != nil {
		return nil, err
	}
	s.sb.WriteString(" FROM ")
	// Table
	s.buildTable(s.table)
	// Where
	if len(s.where) > 0 {
		s.sb.WriteString(" WHERE ")
		if err := s.builder.Build(s.where...); err != nil {
			return nil, err
		}
	}
	// GroupBy
	s.buildGroupItem()
	// Having
	if len(s.having) > 0 {
		s.sb.WriteString(" HAVING ")
		if err := s.builder.Build(s.having...); err != nil {
			return nil, err
		}
	}

	s.buildOrderItem()

	if s.limit > 0 {
		s.sb.WriteString(s.builder.dialect.buildLimit(s.offset, s.limit))
	}
	s.sb.WriteByte(';')
	return &Query{SQL: s.sb.String(), Args: s.args}, nil
}
func (s *Selector[T]) buildTable(table TableReference) error {
	switch tab := table.(type) {
	case nil:
		s.writeWrap(s.model.TableName)
	case Table:
		model, err := s.builder.r.Get(tab.entity)
		if err != nil {
			return err
		}
		s.writeWrap(model.TableName)
		if tab.alias != "" {
			s.sb.WriteString(" AS ")
			s.writeWrap(tab.alias)
		}
	case Join:
		s.buildJoin(tab)
	case SubQuery:
		s.sb.WriteByte('(')
		q, err := tab.s.Build()
		if err != nil {
			return err
		}
		s.sb.WriteString(q.SQL[:len(q.SQL)-1])
		s.sb.WriteByte(')')
		if alias := tab.tableAlias(); alias != "" {
			s.sb.WriteString(" AS ")
			s.writeWrap(alias)
		}
		s.appendArgs(q.Args...)
	default:
		return errs.ErrNoRows
	}
	return nil
}

func (s *Selector[T]) buildJoin(tab Join) error {
	s.sb.WriteByte('(')
	if err := s.buildTable(tab.left); err != nil {
		return err
	}
	s.sb.WriteString(" ")
	s.sb.WriteString(tab.typ)
	s.sb.WriteString(" ")
	if err := s.buildTable(tab.right); err != nil {
		return err
	}
	if len(tab.using) > 0 {
		s.sb.WriteString(" USING (")
		for i, col := range tab.using {
			if i > 0 {
				s.sb.WriteByte(',')
			}
			err := s.buildColumn(Column{name: col}, false)
			if err != nil {
				return err
			}
		}
		s.sb.WriteString(")")
	}
	if len(tab.on) > 0 {
		s.sb.WriteString(" ON ")
		err := s.builder.Build(tab.on...)
		if err != nil {
			return err
		}
	}
	s.sb.WriteByte(')')
	return nil
}
func (s *Selector[T]) buildColumn(c Column, useAlias bool) error {
	err := s.builder.buildColumn(c.table, c.name)
	if err != nil {
		return err
	}
	if useAlias {
		s.buildAs(c.alias)
	}
	return nil
}

func (s *Selector[T]) buildSelectItem() error {
	if len(s.expr) == 0 {
		s.sb.WriteString("*")
		return nil
	}
	for i, expr := range s.expr {
		if i > 0 {
			s.sb.WriteString(", ")
		}
		switch expression := expr.(type) {
		case Column:
			if err := s.buildColumn(expression, true); err != nil {
				return err
			}
		case Aggregate:
			// aggregate := col.(Aggregate)
			fd, ok := s.model.FieldMap[expression.arg]
			if !ok {
				return errs.NewErrUnknownField(expression.arg)
			}
			s.sb.WriteString(expression.fn)
			s.sb.WriteString("(")
			s.writeWrap(fd.ColName)
			s.sb.WriteString(")")
			if expression.alias != "" {
				s.sb.WriteString(" AS ")
				s.writeWrap(expression.alias)
			}
		case RawExpression:
			s.sb.WriteString(expression.raw)
			s.appendArgs(expression.args...)
		}
	}
	return nil
}

func (s *Selector[T]) buildGroupItem() error {
	if len(s.group) == 0 {
		return nil
	}
	s.sb.WriteString(" GROUP BY ")
	for i, item := range s.group {
		if i > 0 {
			s.sb.WriteString(", ")
		}
		switch expression := item.(type) {
		case Column:
			fd, ok := s.model.FieldMap[expression.name]
			if !ok {
				return errs.NewErrUnknownField(expression.name)
			}
			if expression.alias != "" {
				s.writeWrap(expression.alias)
			} else {
				s.writeWrap(fd.ColName)
			}
		case RawExpression:
			s.sb.WriteString(expression.raw)
			s.appendArgs(expression.args...)
		}
	}
	return nil
}

func (s *Selector[T]) buildOrderItem() error {
	if len(s.order) == 0 {
		return nil
	}
	s.sb.WriteString(" ORDER BY ")
	for i, item := range s.order {
		if i > 0 {
			s.sb.WriteString(", ")
		}
		switch expression := item.(type) {
		case OrderSC:
			fd, ok := s.model.FieldMap[expression.item.name]
			if !ok {
				return errs.NewErrUnknownField(expression.item.name)
			}
			if expression.item.alias != "" {
				s.writeWrap(expression.item.alias)
			} else {
				s.writeWrap(fd.ColName)
			}
			if expression.asc {
				s.sb.WriteString(" ASC")
			}
		}
	}
	return nil
}
func (s *Selector[T]) Select(expr ...Selectable) *Selector[T] {
	s.expr = expr
	return s
}

func (s *Selector[T]) From(table TableReference) *Selector[T] {
	s.table = table
	return s
}

func (s *Selector[T]) Where(predicate ...Predicate) *Selector[T] {
	s.where = predicate
	return s
}

func (s *Selector[T]) Having(predicate ...Predicate) *Selector[T] {
	s.having = predicate
	return s
}

func (s *Selector[T]) GroupBy(group ...Groupable) *Selector[T] {
	s.group = group
	return s
}

func (s *Selector[T]) OrderBy(order ...Orderable) *Selector[T] {
	s.order = order
	return s
}

func (s *Selector[T]) Limit(offset, limit int) *Selector[T] {
	s.offset = offset
	s.limit = limit
	return s
}

func (s *Selector[T]) Get(ctx context.Context) (*T, error) {
	res := get[T](ctx, s.core, s.session, &QueryContext{
		Builder: s,
		Type:    "SELECT",
	})
	if res.Err != nil {
		return nil, res.Err
	}
	return res.Result.(*T), nil
}

func (s *Selector[T]) GetMulti(ctx context.Context) ([]*T, error) {
	//TODO implement me
	panic("implement me")
}
