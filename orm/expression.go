package orm

// Expression where 的顶级抽象
type Expression interface {
	expr()
}

type RawExpression struct {
	raw  string
	args []any
}

func Raw(raw string, args ...any) RawExpression {
	return RawExpression{raw: raw, args: args}
}

func (r RawExpression) selectable() {}
func (r RawExpression) groupable()  {}
func (r RawExpression) expr()       {}

func (r RawExpression) AsPredicate() Predicate {
	return Predicate{left: r}
}

func exprOf(e any) Expression {
	switch exp := e.(type) {
	case Expression:
		return exp
	default:
		return valueOf(exp)
	}
}
