package netx_test

import (
	"context"
	"geekgo/micro/netx"
	"net"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type inner struct {
	closedCallTime int
}

func (i *inner) Close() error {
	i.closedCallTime++
	return nil
}
func (i *inner) Read(b []byte) (n int, err error)   { return }
func (i *inner) Write(b []byte) (n int, err error)  { return }
func (i *inner) LocalAddr() net.Addr                { return nil }
func (i *inner) RemoteAddr() net.Addr               { return nil }
func (i *inner) SetDeadline(t time.Time) error      { return nil }
func (i *inner) SetReadDeadline(t time.Time) error  { return nil }
func (i *inner) SetWriteDeadline(t time.Time) error { return nil }

func TestPool(t *testing.T) {
	const CNT = 100
	const IdleCnt = 10
	const MaxCnt = 50

	conn := &inner{}

	connectionCount := 0
	var factory = func() (net.Conn, error) {
		connectionCount++
		return conn, nil
	}

	pool, err := netx.NewPool(IdleCnt, MaxCnt, time.Second*15, factory)
	require.NoError(t, err)
	require.NotNil(t, pool)

	ctx := context.Background()

	wg := sync.WaitGroup{}
	wg.Add(CNT)
	for i := 0; i < CNT; i++ {
		go func() {
			conn, err := pool.Get(ctx)
			require.NoError(t, err)
			time.Sleep(time.Second * 1)
			pool.Put(ctx, conn)
			wg.Done()
		}()
	}
	wg.Wait()
	assert.Equal(t, MaxCnt, connectionCount)
	assert.Equal(t, MaxCnt-IdleCnt, conn.closedCallTime)
	assert.Equal(t, IdleCnt, pool.ActiveCnt())
}

func TestPool_TimeOut(t *testing.T) {
	const CNT = 100 * 2
	const IdleCnt = 10
	const MaxCnt = 50

	conn := &inner{}

	connectionCount := 0
	var factory = func() (net.Conn, error) {
		connectionCount++
		return conn, nil
	}

	pool, err := netx.NewPool(IdleCnt, MaxCnt, time.Second*15, factory)
	require.NoError(t, err)
	require.NotNil(t, pool)

	ctx := context.Background()

	wg := sync.WaitGroup{}
	wg.Add(CNT / 2)
	for i := 0; i < CNT/2; i++ {
		go func() {
			conn, err := pool.Get(ctx)
			require.NoError(t, err)
			time.Sleep(time.Second * 1)
			pool.Put(ctx, conn)
			wg.Done()
		}()
	}
	wg.Wait()

	time.Sleep(time.Second * 15)

	wg2 := sync.WaitGroup{}
	wg2.Add(CNT / 2)
	for i := 0; i < CNT/2; i++ {
		go func() {
			conn, err := pool.Get(ctx)
			require.NoError(t, err)
			time.Sleep(time.Second * 1)
			pool.Put(ctx, conn)
			wg2.Done()
		}()
	}
	wg2.Wait()
	assert.Equal(t, 2*MaxCnt, connectionCount)
	assert.Equal(t, 2*MaxCnt-IdleCnt, conn.closedCallTime)
	assert.Equal(t, IdleCnt, pool.ActiveCnt())
}
