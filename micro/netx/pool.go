package netx

import (
	"context"
	"net"
	"sync"
	"time"
)

type idleConn struct {
	conn net.Conn
	last time.Time
}

type Pool struct {
	idleConn  chan *idleConn // 空闲连接
	maxCnt    int            // 最大连接数量
	activeCnt int            // 当前活跃连接数量
	timeout   time.Duration
	queue     []chan net.Conn

	factory func() (net.Conn, error)
	lock    sync.Mutex
}

func NewPool(idleCnt, maxCnt int, timeout time.Duration, factory func() (net.Conn, error)) (*Pool, error) {
	pool := &Pool{
		idleConn: make(chan *idleConn, idleCnt),
		maxCnt:   maxCnt,
		timeout:  timeout,
		factory:  factory,
	}
	return pool, nil
}

func (p *Pool) Get(ctx context.Context) (net.Conn, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for {
		select {
		case c := <-p.idleConn:
			if c.last.Add(p.timeout).Before(time.Now()) {
				p.lock.Lock()
				c.conn.Close()
				p.activeCnt--
				p.lock.Unlock()
				continue
			}
			return c.conn, nil
		default:
			p.lock.Lock()
			if p.activeCnt >= p.maxCnt {
				request := make(chan net.Conn, 1)
				p.queue = append(p.queue, request)
				p.lock.Unlock()

				select {
				case <-ctx.Done():
					go func() {
						conn := <-request
						p.Put(context.Background(), conn)
					}()
					return nil, ctx.Err()
				case c := <-request:
					return c, nil
				}
			}

			defer p.lock.Unlock()
			conn, err := p.factory()
			if err == nil {
				p.activeCnt++
			}
			return conn, err
		}
	}

}

func (p *Pool) Put(ctx context.Context, conn net.Conn) error {
	p.lock.Lock()
	if wqLen := len(p.queue); wqLen != 0 {
		request := p.queue[0]
		p.queue = p.queue[1:]
		// copy(p.queue[:wqLen-1], p.queue[1:])
		p.lock.Unlock()

		request <- conn
		return nil
	}

	idle := &idleConn{conn: conn, last: time.Now()}

	defer p.lock.Unlock()

	select {
	case p.idleConn <- idle:
	default:
		conn.Close()
		p.activeCnt--
	}
	return nil
}

func (p *Pool) ActiveCnt() int {
	return p.activeCnt
}
